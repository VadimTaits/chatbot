const path = require('path');

const sourcePath = path.join(__dirname, 'src');

const paths = {
  dist: path.join(__dirname, 'dist'),
};

const {
  NODE_ENV,
} = process.env;

module.exports = () => {
  const isProd = NODE_ENV === 'production';

  return {
    mode: isProd ? 'production' : 'development',
    context: sourcePath,
    entry: {
      'playground': './playground/index.tsx',
      'nekto.me': './nekto.me.js',
      'chatvdvoem.ru': './chatvdvoem.ru.ts',
      'talkwithstranger.com': './talkwithstranger.com.js',
    },
    output: {
      path: paths.dist,
      filename: '[name].js'
    },
    module: {
      rules: [
        {
          test: [/(\.js)/, /(\.jsx)/, /(\.ts)/, /(\.tsx)/],
          exclude: /node_modules/,
          use: [
            'babel-loader',
          ],
        },

        {
          test: /(\.json)/,
          exclude: /node_modules/,
          use: [
            'json-loader',
          ],
          type: 'javascript/auto',
        },
      ],
    },

    resolve: {
      modules: [sourcePath, 'node_modules'],
      extensions: ['.js', '.jsx', '.ts', '.tsx'],

      alias: {
        react: 'preact/compat',
        'react-dom/test-utils': 'preact/test-utils',
        'react-dom': 'preact/compat',
      },
    },
  };
};
