module.exports = {
  plugins: [
    '@babel/plugin-transform-runtime',
    [
      '@babel/plugin-transform-react-jsx', {
        runtime: "automatic",
        importSource: "preact",
      },
    ],
  ],

  overrides: [
    {
      test: /\.(ts|tsx)$/,

      presets: [
        '@babel/typescript',
      ],
    },
  ],
};
