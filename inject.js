(() => {
  const {
    hostname,
  } = window.location;

  const scriptTag = document.createElement('script');
  scriptTag.setAttribute('src', `http://127.0.0.1:3012/dist/${hostname}.js`);

  document.head.appendChild(scriptTag);
})();
