export type MessageType = {
  time: string;
  isOwn: boolean;
  text: string;
};

export type ChatType = {
  lastTypingTs: number;
  messages: MessageType[];
};
