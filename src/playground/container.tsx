import type {
  FunctionComponent,
} from 'preact';
import {
  useEffect,
  useReducer,
  useCallback,
  useRef,
} from 'preact/hooks';

import initChat from '../core';

import Chat from './component';
import {
  sendMessage,
  sendBotMessage,
  typeMessage,
} from './actions';

import reducer, {
  initialState,
} from './reducer';

import type {
  ChatType,
  MessageType,
} from './types';

const Container: FunctionComponent = () => {
  const [chat, dispatch] = useReducer(reducer, initialState);
  const stateRef = useRef<ChatType>();
  stateRef.current = chat;

  const sendMessageAction = useCallback((message: string) => {
    dispatch(sendMessage(message));
  }, [dispatch]);

  const typeMessageAction = useCallback(() => {
    dispatch(typeMessage());
  }, [dispatch]);

  useEffect(() => {
    initChat({
      sendMessage: (message: string): void => {
        dispatch(sendBotMessage(message));
      },

      getNewMessages: (loadedMessagesLength: number): MessageType[] => {
        const {
          messages,
        } = stateRef.current;

        return messages.slice(loadedMessagesLength, messages.length);
      },

      checkOpoTyping: () => stateRef.current.lastTypingTs + 1000 > Date.now(),

      isChatEnded: () => false,
      tryStartNewChat: () => false,
      close: () => false,
      isOpoSendAllowed: () => false,
      sendOpoMessage: () => false,
      isStatsAllowed: () => false,
      getOpponentHash: () => null,
      typeLetter: () => { },

      debug: true,

      locale: 'ru',
    });

  }, []);

  return (
    <Chat
      chat={chat}
      sendMessage={sendMessageAction}
      typeMessage={typeMessageAction}
    />
  );
};

export default Container;
