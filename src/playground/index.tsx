import { render } from 'preact';

import Chat from './container';

render(
  <Chat />,
  document.getElementById('app'),
);
