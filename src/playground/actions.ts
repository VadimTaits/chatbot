import {
  SEND_MESSAGE,
  SEND_BOT_MESSAGE,
  TYPE_MESSAGE,
} from './action-types';

export type SendMessageActionType = {
  type: typeof SEND_MESSAGE;
  payload: {
    message: string;
  };
};

export const sendMessage = (message: string): SendMessageActionType => ({
  type: SEND_MESSAGE,
  payload: {
    message,
  },
});

export type SendBotMessageActionType = {
  type: typeof SEND_BOT_MESSAGE;
  payload: {
    message: string;
  };
};

export const sendBotMessage = (message: string): SendBotMessageActionType => ({
  type: SEND_BOT_MESSAGE,
  payload: {
    message,
  },
});

type TypeMessageType = {
  type: typeof TYPE_MESSAGE;
};

export const typeMessage = (): TypeMessageType => ({
  type: TYPE_MESSAGE,
});

export type ActionType =
  | SendMessageActionType
  | SendBotMessageActionType
  | TypeMessageType;
