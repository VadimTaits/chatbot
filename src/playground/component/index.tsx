import type {
  FunctionComponent,
} from 'preact';

import type {
  ChatType,
} from '../types';

import Messages from './messages';
import Form from './form';

type ChatProps = {
  chat: ChatType;
  sendMessage: (message: string) => void;
  typeMessage: () => void;
};

const Chat: FunctionComponent<ChatProps> = ({
  chat: {
    messages,
  },

  sendMessage,
  typeMessage,
}) => (
  <div>
    <Messages
      messages={messages}
    />

    <Form
      sendMessage={sendMessage}
      typeMessage={typeMessage}
    />
  </div>
);

export default Chat;
