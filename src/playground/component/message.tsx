import { memo } from 'preact/compat';

import type {
  MessageType,
} from '../types';

type MessageProps = {
  message: MessageType;
};

const Message = memo<MessageProps>(({
  message: {
    isOwn,
    text,
  },
}) => (
  <div>
    <div>
      <i>
        {isOwn ? 'Бот' : 'Не бот'}
      </i>
    </div>
    
    <div>
      {text}
    </div>
  </div>
))

export default Message;
