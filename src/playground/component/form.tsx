import type {
  FunctionComponent,
} from 'preact';
import {
  useState,
  useCallback,
} from 'preact/hooks';

type FormProps = {
  sendMessage: (message: string) => void;
  typeMessage: () => void;
};

const Form: FunctionComponent<FormProps> = ({
  sendMessage,
  typeMessage,
}) => {
  const [lastMessage, setLastMessage] = useState('');
  const [inputValue, setInputValue] = useState('');

  const onInputKeyDown = useCallback((e: KeyboardEvent) => {
    typeMessage();

    if (e.key === 'ArrowUp') {
      setInputValue(lastMessage);
    }
  }, [lastMessage, typeMessage]);
  
  const onChangeInput = useCallback((e: Event) => {
    setInputValue((e.target as HTMLInputElement).value);
  }, []);

  const onSubmit = useCallback((e: Event) => {
    e.preventDefault();

    setInputValue('');
    setLastMessage(inputValue);

    sendMessage(inputValue);
  }, [sendMessage, inputValue]);

  return (
    <form onSubmit={onSubmit}>
      <input
        value={inputValue}
        onChange={onChangeInput}
        onKeyDown={onInputKeyDown}
      />

      <button
        type="submit"
      >
        Send
      </button>
    </form>
  );
}

export default Form;
