import type {
  VNode,
} from 'preact';
import { memo, useEffect } from 'preact/compat';

import Message from './message';

import type {
  MessageType,
} from '../types';

type MessagesProps = {
  messages: MessageType[];
};

const renderMessage = (message: MessageType, index: number): VNode => (
  <Message
    message={message}
    key={index}
  />
);

const Messages = memo<MessagesProps>(({
  messages,
}) => {
  useEffect(() => {
    window.scrollTo(0, document.body.scrollHeight);
  }, [messages]);

  return (
    <div>
      {messages.map(renderMessage)}
    </div>
  );
});

export default Messages;
