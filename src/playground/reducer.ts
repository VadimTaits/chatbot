import {
  SEND_MESSAGE,
  SEND_BOT_MESSAGE,
  TYPE_MESSAGE,
} from './action-types';

import type {
  ActionType,
} from './actions';

import type {
  ChatType,
  MessageType,
} from './types';

export const initialState: ChatType = {
  lastTypingTs: Date.now(),
  messages: [],
};

const createNewMessage = (message: string, isBot: boolean): MessageType => ({
  time: String(Date.now()),
  isOwn: isBot,
  text: message,
});

const chat = (state: ChatType = initialState, action: ActionType): ChatType => {
  switch (action.type) {
    case SEND_MESSAGE:
      return {
        ...state,
        messages: state.messages
          .concat([createNewMessage(action.payload.message, false)]),
      };

    case SEND_BOT_MESSAGE:
      return {
        ...state,
        messages: state.messages
          .concat([createNewMessage(action.payload.message, true)]),
      };

    case TYPE_MESSAGE:
      return {
        ...state,
        lastTypingTs: Date.now(),
      };

    default:
      return state;
  }
}

export default chat;
