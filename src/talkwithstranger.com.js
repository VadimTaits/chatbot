import initChat from './core';

const textInputEl = document.getElementById('chatMessageInput');

function sendMessage(message) {
  textInputEl.value = message;
  window.sendMessage();
}

function getNewMessages(loadedMessagesLength) {
  const messagesEls = document.querySelectorAll('#logwrapper > .messageblock');

  const newMessagesEls = Array.prototype.filter.call(
    messagesEls,
    (messageEl, index) => index >= loadedMessagesLength,
  );

  return newMessagesEls.map((newMessageEl) => {
    const contentEl = newMessageEl.querySelector('.message');

    return {
      time: Date.now(),
      isOwn: !!newMessageEl.querySelector('.youmsg'),
      text: contentEl ?
        contentEl.textContent :
        '*sticker*',
    };
  });
}

function isChatEnded() {
  return !!document.querySelector('.refesh-btn .status-msg');
}

function isStatsAllowed() {
  return false;
}

function getOpponentHash() {
  return '';
}

function tryStartNewChat() {
  const startBtnEl = document.getElementById('chatStartStopButton');

  if (startBtnEl) {
    startBtnEl.click();

    return true;
  }

  return false;
}

function close() {
  return false;
}

function isOpoSendAllowed() {
  return false;
}

function typeLetter() {
  $inputMessage.trigger('input');
}

function sendOpoMessage(message) {
}

initChat({
  sendMessage,
  getNewMessages,
  isChatEnded,
  tryStartNewChat,
  close,
  isOpoSendAllowed,
  sendOpoMessage,
  isStatsAllowed,
  getOpponentHash,
  typeLetter,

  debug: false,

  locale: 'en',
});
