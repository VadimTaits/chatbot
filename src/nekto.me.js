import initChat from './core';

const textInputEl = document.getElementById('text');
const sendButtonEl = document.getElementById('but-send');

function sendMessage(message) {
  const textInputEl = document.querySelector('.emojionearea-editor');
  const sendButtonEl = document.getElementById('sendMessageBtn');

  textInputEl.textContent = message;
  sendButtonEl.click();
}

function getNewMessages(loadedMessagesLength) {
  const messagesEls = document.querySelectorAll('.window_chat_block .mess_block');

  const newMessagesEls = Array.prototype.filter.call(
    messagesEls,
    (messageEl, index) => index >= loadedMessagesLength,
  );

  return newMessagesEls.map((newMessageEl) => {
    const contentEl = newMessageEl.querySelector('.window_chat_dialog_text');

    return {
      time: Date.now(),
      isOwn: newMessageEl.classList.contains('window_chat_dialog_block_self'),
      text: contentEl ?
        contentEl.textContent :
        '*стикер*',
    };
  });
}

function isChatEnded() {
  const statusEndEl = document.querySelector('.status-end');

  if (!statusEndEl) {
    return false;
  }

  return !statusEndEl.classList.contains('hides');
}

// TO DO
function isStatsAllowed() {
  return false;
}

// TO DO
function getOpponentHash() {
  return null;
}

// TO DO
function typeLetter() {
}

function tryStartNewChat() {
  const startBtnEl = document.getElementById('new_talk_but');

  if (startBtnEl) {
    startBtnEl.click();

    return true;
  }

  return false;
}

function close() {
  ChatEngine.leaveDialog(current_dialog);
  closeDialog();

  return true;
}

function isOpoSendAllowed() {
  return false;
}

function sendOpoMessage(message) {
}

initChat({
  sendMessage,
  getNewMessages,
  isChatEnded,
  tryStartNewChat,
  close,
  isOpoSendAllowed,
  sendOpoMessage,
  isStatsAllowed,
  getOpponentHash,
  typeLetter,

  debug: false,

  locale: 'ru',
});
