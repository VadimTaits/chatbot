const re = /[а-яё]+/gim;

export default function processEveryWord(
  str: string,
  mapper: (subword: string, index: number, matchesLength: number) => string,
): string {
  if (!str) {
    return '';
  }

  const matches = str.match(re);

  if (matches && matches.length > 0) {
    const matchesLength = matches.length;

    let index = 0;
    return str.replace(re, (subword) => {
      const processed = mapper(subword, index, matchesLength);

      ++index;

      return processed;
    });
  }

  return str;
}
