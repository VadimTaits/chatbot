export type MatcherType<OptionType> = {
  matchOptions: {
    frequencySum: number;
    option: OptionType;
  }[];
  fallbackOption?: OptionType;
};

export const generateMatcher = <OptionType>(
  frequencies: [number, OptionType][],
  fallbackOption: OptionType = null,
): MatcherType<OptionType> => {
  const matchOptions = frequencies
    .reduce((res, [frequency, option]) => {
      const prevFrequencySum = res.length > 0 ?
        res[res.length - 1].frequencySum :
        0;

      res.push({
        frequencySum: prevFrequencySum + frequency,
        option,
      });

      return res;
    }, []);

  return {
    matchOptions,
    fallbackOption,
  };
}

export const getRandomOption = <OptionType>({
  matchOptions,
  fallbackOption,
}: MatcherType<OptionType>): OptionType => {
  const randomValue = Math.random();

  for (let i = 0; i < matchOptions.length; ++i) {
    if (randomValue < matchOptions[i].frequencySum) {
      return matchOptions[i].option;
    }
  }

  return fallbackOption;
};
