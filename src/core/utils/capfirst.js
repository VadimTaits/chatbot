export default function capfirst(str) {
  return str[0].toUpperCase() + str.substring(1, str.length);
}
