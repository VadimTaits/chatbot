export default function reverseString(str) {
  return Array.prototype.slice.call(
    str.toLowerCase()
      .replace(/[,?.!)(]/g, '')
  ).reverse().join('')
}
