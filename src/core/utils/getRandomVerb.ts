import verbs from 'core/wordbooks/merge/verbs';

import type {
  LocaleType,
  SexType,
  StrictAmountType,
  VerbCategoryType,
  VerbTimeType,
  VerbMoodType,
  VerbTargetType,
  VerbType,
} from 'core/types';

import getRandomItem from './get-random-item';
import {
  getRandomWordWithAdjective,
} from './get-random-word';

export const getVerbString = (
  locale: LocaleType,
  verb: VerbType,
  category: VerbCategoryType,
  time: VerbTimeType,
  mood: VerbMoodType,
  target: VerbTargetType,
  targetSex: SexType,
  amount: StrictAmountType,
): string => {
  if (locale === 'ru') {
    const stringsByCategory = getRandomItem(verb[category]);

    if (mood === 'imperative') {
      return stringsByCategory[3][0];
    }

    switch (time) {
      case 'past':
        if (amount === 'multiple' || targetSex === 'multiple') {
          return stringsByCategory[2][3];
        }

        switch (targetSex) {
          case 'm':
            return stringsByCategory[2][0];

          case 'f':
            return stringsByCategory[2][1];

          case 'a':
            return stringsByCategory[2][2];

          default:
            throw new Error('Unknow sex');
        }

      case 'present':
        switch (target) {
          case 'i':
            return stringsByCategory[1][0];

          case 'you':
            return stringsByCategory[1][1];

          case 'he':
          case 'she':
          case 'it':
            return stringsByCategory[1][2];

          case 'they':
            return stringsByCategory[1][3];

          default:
            throw new Error('Unknow target');
        }

      case 'future':
        if (category === 'continous') {
          const infinitive = stringsByCategory[0][0];

          switch (target) {
            case 'i':
              return `буду ${infinitive}`;

            case 'you':
              return `будешь ${infinitive}`;

            case 'he':
            case 'she':
            case 'it':
              return `будет ${infinitive}`;

            case 'they':
              return `будут ${infinitive}`;

            default:
              throw new Error('Unknow target');
          }
        }

        switch (target) {
          case 'i':
            return stringsByCategory[1][0];

          case 'you':
            return stringsByCategory[1][1];

          case 'he':
          case 'she':
          case 'it':
            return stringsByCategory[1][2];

          case 'they':
            return stringsByCategory[1][3];

          default:
            throw new Error('Unknow target');
        }

      default:
        throw new Error('Unknow time');
    }
  }

  return getRandomItem(verb[category])[0][0];
};

export const getRandomSingleVerb = (
  locale: LocaleType,
  category: VerbCategoryType,
  time: VerbTimeType,
  mood: VerbMoodType,
  target: VerbTargetType,
  targetSex: SexType,
  amount: StrictAmountType,
): string => {
  const verbsByLocale = verbs[locale];

  const verb = getRandomItem(verbsByLocale);

  return getVerbString(
    locale,
    verb,
    category,
    time,
    mood,
    target,
    targetSex,
    amount,
  );
};

export const getCurrentVerbWithRandomWord = (
  verb: VerbType,
  locale: LocaleType,
  category: VerbCategoryType,
  time: VerbTimeType,
  mood: VerbMoodType,
  target: VerbTargetType,
  targetSex: SexType,
  amount: StrictAmountType,
) => {
  const verbString = getVerbString(
    locale,
    verb,
    category,
    time,
    mood,
    target,
    targetSex,
    amount,
  );

  if (verb.useWith.length === 0) {
    return verbString;
  }

  const useWithItem = getRandomItem(verb.useWith);

  const useWithString = useWithItem
    .map((useWithItem) => {
      if (typeof useWithItem === 'string') {
        return useWithItem;
      }

      const {
        case: wordCase,
        filters,
        amount: wordAmount,
      } = useWithItem;

      return getRandomWordWithAdjective(
        locale,
        wordCase,
        wordAmount,
        filters,
      );
    })
    .join(' ');

  return `${verbString} ${useWithString}`;
};

export const getRandomVerbWithWord = (
  locale: LocaleType,
  category: VerbCategoryType,
  time: VerbTimeType,
  mood: VerbMoodType,
  target: VerbTargetType,
  targetSex: SexType,
  amount: StrictAmountType,
) => {
  const verbsByLocale = verbs[locale];

  const verb = getRandomItem(verbsByLocale.filter(({ useWith }) => useWith.length > 0));

  return getCurrentVerbWithRandomWord(
    verb,
    locale,
    category,
    time,
    mood,
    target,
    targetSex,
    amount,
  );
};
