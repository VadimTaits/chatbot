import cities from 'core/wordbooks/merge/cities';

import type {
  CityType,
  LocaleType,
  WordCaseType,
} from 'core/types';

import {
  getWordCase,
} from './get-random-word';
import getRandomItem from './get-random-item';

export const pickCityName = (
  city: CityType,
  locale: LocaleType,
  wordCase: WordCaseType,
) => getWordCase(city.name, locale, wordCase);

export const getRandomCity = (
  locale: LocaleType,
  wordCase: WordCaseType = 'i',
): string => {
  const city = getRandomItem(cities[locale]);

  return pickCityName(city, locale, wordCase);
};
