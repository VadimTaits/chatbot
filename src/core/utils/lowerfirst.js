export default function lowerfirst(str) {
  return str[0].toLowerCase() + str.substring(1, str.length);
}
