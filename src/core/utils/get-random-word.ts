import type {
  LocaleType,
  SexType,
  WordType,
  WordFiltersType,
  AdjectiveType,
} from 'core/types';
import words from 'core/wordbooks/merge/words';
import adjectives from 'core/wordbooks/merge/adjectives';

import type {
  WordCaseType,
  StrictAmountType,
  AmountType,
} from 'core/types';

import getRandomItem from './get-random-item';

const casesToIndexesMap: Record<WordCaseType, number> = {
  i: 0,
  r: 1,
  d: 2,
  v: 3,
  t: 4,
  p: 5,
};

const sexToAdjectiveIndexMap: Record<SexType, number> = {
  m: 0,
  f: 1,
  a: 2,
  multiple: 3,
};

const strictifyAmountType = (amountType: AmountType): StrictAmountType => {
  if (amountType === 'any') {
    if (Math.random() < 0.5) {
      return 'single';
    }

    return 'multiple';
  }

  return amountType;
};

const getWordCasesByAmountType = (
  wordContainer: WordType,
  amountType: AmountType,
): string[] => {
  const strictAmountType = strictifyAmountType(amountType);

  switch (strictAmountType) {
    case 'single':
      return wordContainer[0];

    case 'multiple':
      return wordContainer[1];

    default:
      console.error(`Unknown amount type: ${strictAmountType}`);
      return wordContainer[1];
  }
};

const getRandomWordContainer = (
  locale: LocaleType,
  filters: WordFiltersType = null,
): WordType => {
  const filtersKeys = filters
    ? Object.keys(filters)
    : null;

  const wordsArray = filters
    ? words[locale].filter((wordContainer) => {
      const properties = wordContainer[2];

      return filtersKeys.every((filtersKey) => filters[filtersKey].includes(properties[filtersKey]));
    })
    : words[locale];

  return getRandomItem(wordsArray);
};

export const getWordCase = (
  wordByCases: string[],
  locale: LocaleType,
  wordCase: WordCaseType,
): string => {
  const wordIndex = locale === 'ru' ?
    casesToIndexesMap[wordCase] :
    0;

  return wordByCases[wordIndex];
};

export const pickWordFromContainer = (
  wordContainer: WordType,
  locale: LocaleType,
  wordCase: WordCaseType = 'i',
  amountType: AmountType = 'any',
): string => {
  const wordCases = getWordCasesByAmountType(wordContainer, amountType);

  return getWordCase(wordCases, locale, wordCase);
};

export const getRandomWord = (
  locale: LocaleType,
  wordCase: WordCaseType = 'i',
  amountType: AmountType = 'any',
  filters: WordFiltersType = null,
): string => {
  const wordContainer = getRandomWordContainer(locale, filters);

  return pickWordFromContainer(wordContainer, locale, wordCase, amountType);
};

export const pickAdjectiveFromContainer = (
  locale: LocaleType,
  adjectiveContainer: AdjectiveType,
  sex: SexType,
  wordCase: WordCaseType,
  amountType: AmountType,
): string => {
  const strictAmountType = strictifyAmountType(amountType);

  const adjectiveSexIndex = strictAmountType === 'multiple'
    ? 3
    : sexToAdjectiveIndexMap[sex];

  const adjectiveCases = adjectiveContainer[adjectiveSexIndex];

  const adjectiveCaseIndex = locale === 'ru'
    ? casesToIndexesMap[wordCase]
    : 0;

  return adjectiveCases[adjectiveCaseIndex];
};

export const getRandomAdjective = (
  locale: LocaleType,
  sex: SexType,
  wordCase: WordCaseType = 'i',
  amountType: AmountType = 'any',
): string => {
  const adjectiveContainer = getRandomItem(adjectives[locale]);

  return pickAdjectiveFromContainer(
    locale,
    adjectiveContainer,
    sex,
    wordCase,
    amountType,
  );
};

const getAdjectiveForWord = (
  locale: LocaleType,
  wordCase: WordCaseType,
  strictAmountType: StrictAmountType,
  wordContainer: WordType,
): string => {
  const {
    live,
    sex,
  } = wordContainer[2];

  const adjectiveSexIndex = strictAmountType === 'multiple'
    ? 3
    : sexToAdjectiveIndexMap[sex];

  const adjectiveContainer = getRandomItem(adjectives[locale]);

  const adjectiveCases = adjectiveContainer[adjectiveSexIndex];

  const adjectiveCaseIndex = locale === 'ru'
    ? (sex === 'm' && wordCase === 'v')
      ? casesToIndexesMap[live ? 'r' : 'i']
      : casesToIndexesMap[wordCase]
    : 0;

  return adjectiveCases[adjectiveCaseIndex];
};

export const getSelectedWordWithAdjective = (
  locale: LocaleType,
  wordContainer: WordType,
  wordCase: WordCaseType = 'i',
  amountType: AmountType = 'any',
): string => {
  const strictAmountType = strictifyAmountType(amountType);

  const word = pickWordFromContainer(wordContainer, locale, wordCase, strictAmountType);

  const hasAdjective = Math.random() > 0.75;

  if (!hasAdjective) {
    return word;
  }

  const adjective = getAdjectiveForWord(
    locale,
    wordCase,
    strictAmountType,
    wordContainer,
  );

  return `${adjective} ${word}`;
};

export const getRandomWordWithAdjective = (
  locale: LocaleType,
  wordCase: WordCaseType = 'i',
  amountType: AmountType = 'any',
  filters: WordFiltersType = null,
): string => {
  const wordContainer = getRandomWordContainer(locale, filters);

  return getSelectedWordWithAdjective(
    locale,
    wordContainer,
    wordCase,
    amountType,
  );
};

export const getRandomAdjectiveWithSelectedWord = (
  locale: LocaleType,
  wordContainer: WordType,
  wordCase: WordCaseType = 'i',
  amountType: AmountType = 'any',
): string => {
  const strictAmountType = strictifyAmountType(amountType);

  const word = pickWordFromContainer(wordContainer, locale, wordCase, strictAmountType);

  const adjective = getAdjectiveForWord(
    locale,
    wordCase,
    strictAmountType,
    wordContainer,
  );

  return `${word} ${adjective}`;
};

export const getRandomAdjectiveWithWord = (
  locale: LocaleType,
  wordCase: WordCaseType = 'i',
  amountType: AmountType = 'any',
  filters: WordFiltersType = null,
): string => {
  const wordContainer = getRandomWordContainer(locale, filters);

  return getRandomAdjectiveWithSelectedWord(
    locale,
    wordContainer,
    wordCase,
    amountType,
  );
};
