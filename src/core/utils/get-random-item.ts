const getRandomItem = <ItemType>(
  itemsArray: ItemType[],
): ItemType => itemsArray[Math.floor(Math.random() * itemsArray.length)];

export default getRandomItem;
