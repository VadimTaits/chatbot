import {
  generateMatcher,
  getRandomOption,
} from '../utils/get-random-option';
import type {
  MatcherType,
} from '../utils/get-random-option';

import type {
  SmileType,
  EmotionType,
  DecoratorType,
} from '../types';

const variations: Record<SmileType, Record<EmotionType, MatcherType<string>>> = {
  bracket: {
    default: generateMatcher([
      [0.3, ')'],
    ], ''),

    laught: generateMatcher([
      [0.1, ')'],
      [0.15, '))'],
      [0.15, ')))'],
      [0.15, ' ахах'],
      [0.15, ' хаха'],
      [0.15, ' ахаха'],
      [0.15, ' ахахах'],
    ], ''),

    sad: generateMatcher([
      [0.3, '('],
      [0.6, '(('],
      [0.1, '((('],
    ], ''),

    not_understand: generateMatcher([
      [0.3, ')'],
    ], ''),
  },

  emoji: {
    default: generateMatcher([
      [0.1, '😀️'],
      [0.1, '😃️'],
      [0.05, '😄️'],
      [0.02, '😉️'],
      [0.02, '😎'],
    ], ''),

    laught: generateMatcher([
      [0.05, '😆'],
      [0.05, '😆😆'],
      [0.05, '😆😆😆'],
      [0.05, '😂️'],
      [0.05, '😂️😂️'],
      [0.05, '🤣️'],
      [0.05, '🤣️🤣️'],
      [0.05, '🤣️🤣️🤣️'],
      [0.05, '😜'],
      [0.05, '😜😜'],
      [0.05, '😛'],
      [0.05, '😛'],
      [0.05, '🙃'],
      [0.05, ' ахах'],
      [0.05, ' хаха'],
      [0.05, ' ахаха'],
      [0.05, ' ахахах'],
    ], '😆😆😆'),

    sad: generateMatcher([
      [0.2, '😥️'],
      [0.2, '😫️'],
      [0.2, '😭️'],
      [0.2, '😿️'],
    ], ''),

    not_understand: generateMatcher([
      [0.4, '🤔'],
      [0.1, '🙄️'],
      [0.1, '😑️'],
      [0.1, '😶️'],
      [0.1, '😐️'],
    ], ''),
  },
};

const bracketMatcher = generateMatcher([
  [0.3, ')'],
  [0.05, '))'],
  [0.05, ')))'],
  [0.05, '('],
  [0.01, '*'],
  [0.01, ':)'],
  [0.01, '(:'],
  [0.01, ':*'],
  [0.01, ':3'],
], '');

const emojiMatcher = generateMatcher([
  [0.1, '😉'],
  [0.05, '😉😉'],
  [0.025, '😉😉😉'],
  [0.1, '😆'],
  [0.05, '😆😆'],
  [0.025, '😆😆😆'],
  [0.01, '😜'],
  [0.01, '🙂'],
  [0.01, '🙃'],
  [0.01, '🤔'],
  [0.01, '😛'],
  [0.01, '😍'],
  [0.01, '😎'],
], '');

export const smileDecorator: DecoratorType = (
  sendMessage,
  {
    locale,
    smileType,
  },
) => {
  if (locale === 'en') {
    return sendMessage;
  }

  switch (smileType) {
    case 'bracket':
    case 'emoji':
      return {
        ...sendMessage,
        message: sendMessage.message + getRandomOption(variations[smileType][sendMessage.emotion || 'default']),
      };

    default:
      return sendMessage;
  }
};
