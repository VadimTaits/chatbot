import getRandomItem from '../utils/get-random-item';
import processEveryWord from '../utils/process-every-word';

import type {
  DecoratorType,
  SessionType,
} from '../types';

const lettersForReplace = {
  'з': ['с'],
  'с': ['з'],
  'н': ['нн'],
  'а': ['о'],
  'о': ['а'],
  'е': ['и', 'э'],
  'ш': ['ж'],
  'ж': ['ш'],
  'д': ['т'],
};

const makeGrammarError = (
  text: string,
  maxErrorsNumber: number,
  errorProbability: number,
): string => {
  if (maxErrorsNumber === 0) {
    return text;
  }

  let errorsNumber = 0;
  let replaced = false;
  let resText = '';

  for (let i = 0; i < text.length; ++i) {
    const letter = text[i];

    if (replaced) {
      resText += letter;
    } else {
      if (lettersForReplace[letter]) {
        if (Math.random() < errorProbability) {
          resText += getRandomItem(lettersForReplace[letter]);

          ++errorsNumber;

          if (errorsNumber === maxErrorsNumber) {
            replaced = true;
          }
        } else {
          resText += letter;
        }
      } else {
        resText += letter;
      }
    }
  }

  return resText;
};

const grammarErrorsForOneWord = (
  text: string,
  {
    grammarErrorFrequency,
    grammarErrorProbability,
  }: SessionType,
) => {
  const maxErrorsNumber = Math.floor(Math.random() * text.length / grammarErrorFrequency);

  const res = makeGrammarError(text, maxErrorsNumber, grammarErrorProbability);

  return res;
}

export const grammarErrorsDecorator: DecoratorType = (
  sendMessage,
  session,
) => {
  const {
    isAngryAllowed,
    isAngry,
    locale,
  } = session;

  if (locale === 'en' || (isAngryAllowed && isAngry)) {
    return sendMessage;
  }

  return {
    ...sendMessage,
    message: processEveryWord(sendMessage.message, (subword) => grammarErrorsForOneWord(subword, session)),
  };
}
