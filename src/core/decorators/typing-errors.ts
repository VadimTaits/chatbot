import getRandomItem from '../utils/get-random-item';
import processEveryWord from '../utils/process-every-word';

import type {
  DecoratorType,
  SessionType,
} from '../types';

const lettersForReplace = {
  'й': ['ц'],
  'ц': ['й', 'ц'],
  'у': ['ц', 'к'],
  'к': ['у', 'е'],
  'е': ['к', 'н'],
  'н': ['е', 'г'],
  'г': ['н', 'ш'],
  'ш': ['г', 'щ'],
  'щ': ['ш', 'з'],
  'з': ['щ', 'х'],
  'х': ['з', 'ъ'],
  'ъ': ['х'],
  'ф': ['ы'],
  'ы': ['ф', 'в'],
  'в': ['ы', 'а'],
  'а': ['в', 'п'],
  'п': ['а', 'р'],
  'р': ['п', 'о'],
  'о': ['р', 'л'],
  'л': ['о', 'д'],
  'д': ['л', 'ж'],
  'ж': ['д', 'э'],
  'э': ['ж', '\\'],
  'я': ['ч'],
  'ч': ['я', 'с'],
  'с': ['ч', 'м'],
  'м': ['с', 'и'],
  'и': ['м', 'т'],
  'т': ['и', 'ь'],
  'ь': ['т', 'б'],
  'б': ['ь', 'ю'],
  'ю': ['б', '.'],
};

const makeTypingError = (
  text: string,
  maxErrorsNumber: number,
  typingErrorProbability: number,
): string => {
  if (maxErrorsNumber === 0) {
    return text;
  }

  let errorsNumber = 0;
  let replaced = false;
  let isFirstLetter = true;
  let resText = '';

  for (let i = 0; i < text.length; ++i) {
    const letter = text[i];

    if (replaced) {
      resText += letter;
    } else {
      const randomValue = Math.random();

      if (lettersForReplace[letter]) {
        if (isFirstLetter) {
          isFirstLetter = false;
          resText += letter;
        } else {
          if (randomValue < typingErrorProbability * 3 / 4) {
            if (randomValue < typingErrorProbability / 2) {
              resText += letter;
            }

            resText += getRandomItem(lettersForReplace[letter]);
            ++errorsNumber;
          } else if (randomValue > typingErrorProbability) {
            ++errorsNumber;
          } else {
            resText += letter;
          }
        }
      } else {
        isFirstLetter = true;
        resText += letter;
      }
    }

    replaced = errorsNumber === maxErrorsNumber;
  }

  return resText;
};

const typingErrorsForOneWord = (
  text: string,
  {
    typingErrorFrequency,
    typingErrorProbability,
  }: SessionType,
): string => {
  const maxErrorsNumber = Math.floor(Math.random() * text.length / typingErrorFrequency);

  const res = makeTypingError(text, maxErrorsNumber, typingErrorProbability);

  return res;
};

export const typingErrorsDecorator: DecoratorType = (
  sendMessage,
  session,
) => {
  const {
    isAngryAllowed,
    isAngry,
    locale,
  } = session;

  if (locale === 'en' || (isAngryAllowed && isAngry)) {
    return sendMessage;
  }

  return {
    ...sendMessage,
    message: processEveryWord(sendMessage.message, (subword) => typingErrorsForOneWord(subword, session)),
  };
}
