import capfirst from 'core/utils/capfirst';
import lowerfirst from 'core/utils/lowerfirst';

import type {
  DecoratorType,
} from '../types';

export const firstLetterDecorator: DecoratorType = (sendMessage, { isCapfirst }) => ({
  ...sendMessage,
  message: isCapfirst ?
    capfirst(sendMessage.message) :
    lowerfirst(sendMessage.message),
});
