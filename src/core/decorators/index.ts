import { firstLetterDecorator } from './first-letter';
import { grammarErrorsDecorator } from './grammar-errors';
import { typingErrorsDecorator } from './typing-errors';
import { smileDecorator } from './smiles';

import type {
  DecoratorType,
} from '../types';

export const decorators: DecoratorType[] = [
  firstLetterDecorator,
  grammarErrorsDecorator,
  typingErrorsDecorator,
  smileDecorator,
];
