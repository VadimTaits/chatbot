import getRandomItem from './utils/get-random-item';

import { manNamesLowerFlatten, womanNamesLowerFlatten } from './wordbooks/ru/names';

const STORAGE_KEY = 'ALL_MESSAGES';
const ALLOWED_KEY = 'RANDOM_MESSAGE_ALLOWED';

const isAllowedFrom = 50;
const messagesLimit = 500;

function getStoredMessages() {
  const storedMessagesJSON = localStorage[STORAGE_KEY];

  if (!storedMessagesJSON) {
    return [];
  }

  let storedMessages;
  try {
    storedMessages = JSON.parse(storedMessagesJSON);
  } catch (e) {
    storedMessages = [];
  }

  return storedMessages;
}

const blacklistSubstrings = [
  'я парень',
  'мальчик',
  'я м',
  'гей',
  'ебал',
  'ебат',
  'ебут',
  'выебу',
  'въеб',
  'лесби',
  'лизби',
  'шлюх',
  'мать',
  'мамка',
  'мама',
  'сдохне',
  'иди нах',
  'путин',
  'пописаешь',
  'пописай',
  'сперма',
  'пошлый',
  'пошлая',
  'пошлую',
  'пошлос',
  'пошлен',
  'госпожа',
  'хуй',
  'болт',
  'пизд',
  'бляд',
  'траха',
  'письк',
  'писю',
  'попк',
  'ебобо',
  'долбое',
  'долбоё',
  'дроч',
  'член',
  'пенис',
  'сиськи',
  'сисечки',
  'грудь',
  'порно',
  'вирт',
  'кончи',
  'минет',
  'куни',
  'секс',
  'ласкаеш',
  'лизат',
  'сосал',
  'сосат',
  'саснул',
  'разден',
  'раздев',
  'пошали',
  'chlen',
  'попу',
  'жопу',
  'gospodin',
  'virt',
  'hui',
  'пидр',
  'пидор',
  'педик',
  '*стикер*',
  'tits',
  'sissy',
  'т-elег-рam',
  'fiagro',
  'olyaatihonova',
  'craypierrot',
  'скинь',
  'фото',
  'трахну',
  'ссал',
  'срал',
  'анал',
  'разврат',
  'кончу',
];

const blacklistStartStrings = [
  'парень',
  'мальчик',
  'ищу',
  'привет ищу',
  'м ',
];

function filterMessageText(textLower) {
  if (textLower.length < 3) {
    return false;
  }

  if (manNamesLowerFlatten.some((name) => textLower.includes(name))) {
    return false;
  }

  if (womanNamesLowerFlatten.some((name) => textLower.includes(name))) {
    return false;
  }

  if (blacklistSubstrings.some((str) => textLower.includes(str))) {
    return false;
  }

  if (blacklistStartStrings.some((str) => textLower.indexOf(str) === 0)) {
    return false;
  }

  return true;
}

function saveMessages(messages) {
  localStorage[STORAGE_KEY] = JSON.stringify(messages);
}

export function pushMessages(messages) {
  const storedMessages = getStoredMessages();
  const messagesForPush = messages
    .filter((message) =>
      !message.isOwn && filterMessageText(message.text.toLowerCase())
    )
    .map((message) => message.text);

  const newStoredMessages = messagesForPush
    .concat(storedMessages)
    .slice(0, messagesLimit);

  saveMessages(newStoredMessages);

  if (newStoredMessages.length >= isAllowedFrom) {
    localStorage[ALLOWED_KEY] = '1';
  } else {
    delete localStorage[ALLOWED_KEY];
  }
}

export function isAllowed() {
  return localStorage[ALLOWED_KEY] === '1';
}

export function getRandomMessage() {
  const storedMessages = getStoredMessages();

  return getRandomItem(storedMessages);
}

window.cleanBadMessagesBySubstring = (substr) => {
  const storedMessages = getStoredMessages();

  const substrLower = substr.toLowerCase();

  const filteredMessages = storedMessages
    .filter((message) => !message.toLowerCase().includes(substrLower));

  saveMessages(filteredMessages);
};

window.cleanBadMessages = () => {
  const storedMessages = getStoredMessages();

  const filteredMessages = storedMessages
    .filter((message) => filterMessageText(message.toLowerCase()));

  saveMessages(filteredMessages);
};
