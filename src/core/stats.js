const STORAGE_KEY = 'STATS';

export function getStoredStats() {
  const storedStatsJSON = localStorage[STORAGE_KEY];

  if (!storedStatsJSON) {
    return {};
  }

  let storedStats;
  try {
    storedStats = JSON.parse(storedStatsJSON);
  } catch (e) {
    storedStats = {};
  }

  return storedStats;
}

function saveStats(stats) {
  localStorage[STORAGE_KEY] = JSON.stringify(stats);
}

const defaultStatsByUser = {
  count: 0,
  durations: [],
  payload: {},
};

export function onChatStart(chat) {
  if (!chat.isStatsAllowed()) {
    return;
  }

  const stats = getStoredStats();
  const userHash = chat.getOpponentHash();

  if (!userHash) {
    return;
  }

  if (!stats[userHash]) {
    stats[userHash] = defaultStatsByUser;
  }

  ++stats[userHash].count;

  saveStats(stats);
}

export function onChatEnd(chat, userHash, tsDiff) {
  console.log('on chat end', chat, userHash, tsDiff)
  if (!chat.isStatsAllowed() || !userHash) {
    return;
  }

  const stats = getStoredStats();

  if (!stats[userHash]) {
    stats[userHash] = defaultStatsByUser;
  }

  console.log('stored stats', JSON.stringify(stats[userHash]));

  stats[userHash].durations.push(Math.floor(tsDiff / 1000));

  console.log('new stored stats', JSON.stringify(stats[userHash]));

  saveStats(stats);
}
