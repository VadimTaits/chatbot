export type LocaleType = 'ru' | 'en';

export type SmileType = 'bracket' | 'emoji';

export type SexType = 'f' | 'm' | 'a' | 'multiple';

export type ContainableType = 'big' | 'small' | 'none';

export type FoodType = 'none' | 'eat' | 'drink';

export type WordCaseType =
  | 'i'
  | 'r'
  | 'd'
  | 'v'
  | 't'
  | 'p';

export type StrictAmountType =
  | 'single'
  | 'multiple';

export type AmountType = StrictAmountType | 'any';

export type WordPayloadType = {
  live: boolean;
  sex: SexType;
  containable: ContainableType;
  food: FoodType;
  costume: boolean;
  shoes: boolean;
  canLearnIn?: boolean;
  canWorkIn?: boolean;
};

export type WordType = [
  string[],
  string[],
  WordPayloadType,
];

export type WordFiltersType = {
  live?: boolean[];
  sex?: SexType[];
  containable?: ContainableType[];
  food?: FoodType[];
  costume?: boolean[];
  shoes?: boolean[];
  canLearnIn?: boolean[];
  canWorkIn?: boolean[];
};

export type AdjectiveType = [
  string[],
  string[],
  string[],
  string[],
];

export type CityType = {
  name: string[];
  sex: SexType;
  tzOffset: number;
};

export type MessageLexems = {
  allWords: string[];
  smallWords: string[];
  bigWords: string[];
  words: number[];
  adjectives: number[];
  verbs: number[];
  manNames: number[];
  womanNames: number[];
  cities: number[];
};

export type SessionType = {
  debug: boolean;

  name: string[];
  nameLower: string[];
  age: number;

  activity?: string;
  activityExpired?: number;
  city: CityType;
  locale: LocaleType;
  parsedLastMessages: MessageLexems[];
  minPrintTimeout: number;
  additionalPrintTimeout: number;
  minLetterTimeout: number;
  additionalLetterTimeout: number;
  lastSolver: SolverType<any>;
  lastSolverPayload: any;
  messagesCount: number;
  rememberMessagesCount: number;
  isOtherBot: boolean;
  isAngryAllowed: boolean;
  isAngryAllowedForStupid: boolean;
  isAngry: boolean;
  isCapfirst: boolean;
  typingErrorFrequency: number;
  typingErrorProbability: number;
  grammarErrorFrequency: number;
  grammarErrorProbability: number;
  waitingTimeMs: number;
  waitingTimeFromFirstMessageTs: number;
  smileType?: SmileType;
  lastActivityTs?: number;
  lastMessageFrom?: 'me' | 'opo';

  opponent: {
    age?: number;
    surnames: string[];
  };

  /**
   * TO DO: move to solversPayload
   */
  usedQuestions: Record<string, boolean>;
  /**
   * TO DO: move to solversPayload
   */
  usedConundrums: Record<string, boolean>;

  solversPayload: Record<string, Record<string, any>>;

  isOwnMessageSent: boolean;
};

export type EmotionType =
  | undefined /* default */
  | 'default'
  | 'laught'
  | 'sad'
  | 'not_understand';

export type SendMessageType = {
  message: string;
  emotion?: EmotionType;
};

export type FullSendMessageType = string | SendMessageType;

export type SolverCheckType<PayloadType> = (
  textLower: string,
  session: SessionType,
) => PayloadType;

export type SolverAnswerType<PayloadType> = (
  textLower: string,
  session: SessionType,
  payload: PayloadType,
) => FullSendMessageType;

export type RawMessageType = {
  time: string;
  isOwn: boolean;
  text: string;
};

export type SolverType<PayloadType> = {
  check: SolverCheckType<PayloadType>;
  answer: SolverAnswerType<PayloadType>;
  canRepeat?: boolean;
  tryRepeat?: (
    payload: PayloadType,
    textLower: string,
    currentSession: SessionType,
    messages: RawMessageType[],
  ) => PayloadType;

  sendOwnMessage?: (
    send: (message: FullSendMessageType) => void,
    session: SessionType,
  ) => void;
};

export type VerbUseWithWordType = {
  filters: WordFiltersType;
  case: WordCaseType;
  amount?: AmountType;
};

export type VerbUseWithItemType =
  | string
  | VerbUseWithWordType;

export type VerbUseWithType = VerbUseWithItemType[];

export type VerbCategoryType = 'perfect' | 'continous';

export type VerbTimeType = 'future' | 'present' | 'past';

export type VerbMoodType = 'indicative' | 'imperative';

export type VerbTargetType = 'i' | 'you' | 'he' | 'she' | 'it' | 'they';

export type VerbType = {
  /**
   * inf
   * future: i, you, he/she/it, they
   * past: he, she, it, they
   * imperative: you
   */
  perfect: [
    [string],
    [string, string, string, string],
    [string, string, string, string],
    [string],
  ][];
  /**
   * inf
   * present: i, you, he/she/it, they
   * past: he, she, it, they
   * imperative: you
   */
  continous: [
    [string],
    [string, string, string, string],
    [string, string, string, string],
    [string],
  ][];
  useWith: VerbUseWithType[];
};

export type WordsCache = {
  words: string[];
  wordsDict: Record<string, number>;
  adjectives: string[];
  adjectivesDict: Record<string, number>;
  verbs: string[];
  verbsDict: Record<string, number>;
  manNames: string[];
  manNamesDict: Record<string, number>;
  womanNames: string[];
  womanNamesDict: Record<string, number>;
  cities: string[];
  citiesDict: Record<string, number>;
};

export type DecoratorType = (
  sendMessage: SendMessageType,
  session: SessionType,
) => SendMessageType;

export type ChatType = {
  sendMessage: (message: string) => void;
  getNewMessages: (loadedMessagesLength: number) => RawMessageType[];
  isChatEnded: () => boolean;
  tryStartNewChat: () => boolean;
  checkOpoTyping: () => boolean;
  close: () => void;
  isOpoSendAllowed: () => boolean;
  sendOpoMessage?: (message: string) => void;
  isStatsAllowed: () => boolean;
  getOpponentHash?: () => string;
  typeLetter: () => void;

  additionalStyle?: string;

  debug: boolean;

  locale: LocaleType;
};
