import { render } from 'preact';

import getRandomItem from './utils/get-random-item';
import reverseString from './utils/reverse-string';
import { generateMatcher, getRandomOption } from './utils/get-random-option';
import names from './wordbooks/merge/names';
import cities from './wordbooks/merge/cities';

import { solvers } from './solvers';
import { decorators } from './decorators';
import { pushMessages } from './tabs-sync';

import * as stats from './stats';

import ControlPanel from './control-panel';

import { getWordsFromMessage } from './getWordsFromMessage';
import { initWordsCache } from './initWordsCache';

import type {
  ChatType,
  FullSendMessageType,
  SendMessageType,
  SessionType,
  SmileType,
} from './types';

let controlPanelNode;

if (!window.__INITED__) {
  window.__ENABLED__ = true;
  window.__TIMEOUT__ = 1000;
  window.__RESTART_TIMEOUT__ = 4 * 60 * 1000;
  window.__AUTORESET__ = true;

  window.enableBot = () => {
    window.__ENABLED__ = true;
  };

  window.disableBot = () => {
    window.__ENABLED__ = false;
  };

  window.toggleBot = () => {
    window.__ENABLED__ = !window.__ENABLED__;
  };

  controlPanelNode = document.createElement('div');

  document.body.appendChild(controlPanelNode);
  render(
    <ControlPanel
      enabled={window.__ENABLED__}
      autoreset={window.__AUTORESET__}
    />,
    controlPanelNode,
  );
}

window.__INITED__ = true;

function applyDecorators(
  sendMessage: SendMessageType,
  currentSession: SessionType,
): SendMessageType {
  return decorators.reduce<SendMessageType>(
    (res, decorator) => decorator(res, currentSession),
    sendMessage,
  );
}

const capfirstMatcher = generateMatcher([
  [0.5, true],
], false);

const smileMatcher = generateMatcher<SmileType>([
  [0.6, 'bracket'],
  [0.3, 'emoji'],
]);

function initSession(chat: ChatType): SessionType {
  const name = getRandomItem(names[chat.locale].womanNames);
  const nameLower = name
    .map((name) => name.toLowerCase());
  const city = getRandomItem(cities[chat.locale]);

  const age = 18 + Math.floor(Math.random() * 7);

  const grammarErrorFrequency = 4 + Math.floor(Math.random() * 9);
  const typingErrorFrequency = 4 + Math.floor(Math.random() * 18);

  // const savedOpponents = JSON.parse(localStorage.__SAVED__ || '{}');

  const session: SessionType = {
    name,
    nameLower,

    rememberMessagesCount: 2 + Math.round(4 * Math.random()),

    age,

    grammarErrorFrequency,
    grammarErrorProbability: 1 / grammarErrorFrequency,

    typingErrorFrequency,
    typingErrorProbability: 1 / typingErrorFrequency,

    minPrintTimeout: 500 + Math.floor(Math.random() * 1500),
    additionalPrintTimeout: 500 + Math.floor(Math.random() * 4500),

    minLetterTimeout: 200 + Math.floor(Math.random() * 300),
    additionalLetterTimeout: 200 + Math.floor(Math.random() * 300),

    isCapfirst: getRandomOption(capfirstMatcher),
    smileType: getRandomOption(smileMatcher),

    isAngryAllowed: Math.random() < 0.1,
    isAngryAllowedForStupid: Math.random() < 0.2,
    isAngry: false,

    isOtherBot: false,

    debug: chat.debug,

    locale: chat.locale,

    lastSolver: null,
    lastSolverPayload: null,

    usedQuestions: {},
    usedConundrums: {},

    city,

    messagesCount: 0,

    opponent: {
      // ...(savedOpponents[chat.getOpponentHash()]?.opponent || {}),
      surnames: [],
    },

    parsedLastMessages: [],

    lastActivityTs: Date.now(),
    lastMessageFrom: null,
    waitingTimeMs: 5000 + Math.floor(Math.random() * 25000),
    waitingTimeFromFirstMessageTs: 2000 + Math.floor(Math.random() * 8000),

    solversPayload: {},

    isOwnMessageSent: false,
  };

  window.__SESSION__ = session;

  return session;
}

let removed = false;
let chatIndex = 1;

function typeMessage(
  chat: ChatType,
  messageLength: number,
  printTimeout: number,
  letterTimeout: number,
): void {
  const currentChatIndex = chatIndex;

  for (let i = 0; i < messageLength; ++i) {
    setTimeout(() => {
      if (currentChatIndex !== chatIndex) {
        return;
      }

      chat.typeLetter();
    }, printTimeout + letterTimeout * i);
  }
}

window.updateBot = () => {
  removed = true;

  const {
    hostname,
  } = window.location;

  const scriptTag = document.createElement('script');
  scriptTag.setAttribute('src', `http://127.0.0.1:3012/dist/${hostname}.js?${Date.now()}`);

  document.head.appendChild(scriptTag);
};

const customStyleId = 'chatbot-additional-style';
function appendStyle(additionalStyle: string): void  {
  const oldCustomStyle = document.getElementById(customStyleId);

  if (oldCustomStyle) {
    document.head.removeChild(oldCustomStyle);
  }

  if (additionalStyle) {
    const customStyle = document.createElement('style');
    customStyle.id = customStyleId;

    customStyle.appendChild(document.createTextNode(additionalStyle));

    document.head.appendChild(customStyle);
  }
}

export default function initChat(chat: ChatType) {
  appendStyle(chat.additionalStyle);

  const wordsCache = initWordsCache(chat.locale);

  let messageForSend = null;
  let currentSession = window.__SESSION__ || initSession(chat);
  let chatStartTs = Date.now();
  let userHash = chat.getOpponentHash();

  function sendMessage(fullMessage: FullSendMessageType): void {
    if (fullMessage === null) {
      return;
    }

    const sendMessage: SendMessageType = typeof fullMessage === 'string'
      ? {
        message: fullMessage,
      }
      : fullMessage;

    const {
      message,
    } = sendMessage;

    if (message === null || message.trim().length === 0) {
      return;
    }

    const currentChatIndex = chatIndex;

    const printTimeout = currentSession.minPrintTimeout +
      Math.floor(Math.random() * currentSession.additionalPrintTimeout);
    const letterTimeout = currentSession.minLetterTimeout +
      Math.floor(Math.random() * currentSession.additionalLetterTimeout);

    messageForSend = message;

    if (chat.debug) {
      chat.sendMessage(applyDecorators(sendMessage, currentSession).message);
    } else {
      typeMessage(chat, message.length, printTimeout, letterTimeout);

      setTimeout(() => {
        if (currentChatIndex !== chatIndex) {
          return;
        }

        if (messageForSend !== message) {
          return;
        }

        if (!removed) {
          window.__SESSION__.lastActivityTs = Date.now();
          chat.sendMessage(applyDecorators(sendMessage, currentSession).message);
        }

        messageForSend = null;
      }, printTimeout + letterTimeout * message.length);
    }
  }

  let isEnabled = false;
  let messages = [];

  function handleEnd() {
    if (messages.length > 0) {
      messages = [];
    }

    const savedOpponents = chat.isStatsAllowed()
      ? JSON.parse(localStorage.__SAVED__ || '{}')
      : {};

    savedOpponents[chat.getOpponentHash()] = {
      opponent: window.__SESSION__.opponent,
    };

    try {
      localStorage.__SAVED__ = JSON.stringify(savedOpponents);
    } catch (e) {
    }

    currentSession = initSession(chat);
  }

  function checkMessages() {
    const isOpoTyping = chat.checkOpoTyping();
    const newMessages = chat.getNewMessages(messages.length);

    if (isOpoTyping) {
      window.__SESSION__.lastActivityTs = Date.now();
    }

    if (newMessages.length === 0) {
      const {
        lastActivityTs,
        waitingTimeMs,
        waitingTimeFromFirstMessageTs,
        lastMessageFrom,
        isOwnMessageSent,
      } = window.__SESSION__;

      
      if (!isOwnMessageSent) {
        const now = Date.now();
        
        if (
          lastMessageFrom === null && lastActivityTs + waitingTimeFromFirstMessageTs < now
          || lastMessageFrom === 'me' && lastActivityTs + waitingTimeMs < now
        ) {
          for (
            let i = 0, l = solvers.length, solved = false;
            i < l && !solved;
            ++i
          ) {
            const solver = solvers[i];

            if (solver.sendOwnMessage) {
              solver.sendOwnMessage(
                (message) => {
                  solved = true;

                  sendMessage(message);
                },

                currentSession,
              );
            }
          }

          window.__SESSION__.isOwnMessageSent = true;
        }
      }

      return;
    }

    messages = [...messages, ...newMessages];

    if (window.__ENABLED__) {
      newMessages.forEach(({ isOwn, text }) => {
        window.__SESSION__.lastMessageFrom = isOwn ? 'me' : 'opo';

        if (!isOwn) {
          ++window.__SESSION__.messagesCount;
          window.__SESSION__.isOwnMessageSent = false;

          const random = Math.random();

          /* if (random > 0.99) {
            sendMessage('Жоба жваночки врот бевашлю гряжят арбу твой жрот');
          } else if (random > 0.98) {
            sendMessage('Коагаз ямерв ьнечо ен сащ ун😎');
          } else */
          if (random > 0.99) {
            sendMessage(reverseString(text));
          } else {
            const textLower = text.toLowerCase();

            const parsedMessage = getWordsFromMessage(textLower, wordsCache);

            currentSession.parsedLastMessages = [
              parsedMessage,
              ...currentSession.parsedLastMessages,
            ].slice(0, currentSession.rememberMessagesCount);

            const {
              lastSolver,
              lastSolverPayload,
            } = window.__SESSION__;

            if (lastSolver && lastSolverPayload && lastSolver.tryRepeat) {
              const newPayload = lastSolver.tryRepeat(
                lastSolverPayload,
                textLower,
                currentSession,
                messages,
              );

              if (newPayload) {
                window.__SESSION__.lastSolverPayload = newPayload;

                sendMessage(lastSolver.answer(textLower, currentSession, newPayload));

                return;
              }
            }

            for (
              let i = 0, l = solvers.length, solved = false;
              i < l && !solved;
              ++i
            ) {
              const solver = solvers[i];

              if (solver !== lastSolver || solver.canRepeat) {
                const checkPayload = solver.check(textLower, currentSession, messages);

                if (checkPayload) {
                  solved = true;

                  window.__SESSION__.lastSolver = solver;
                  window.__SESSION__.lastSolverPayload = checkPayload;

                  sendMessage(solver.answer(textLower, currentSession, checkPayload));
                }
              }
            }
          }
        }
      });

      if (
        !currentSession.isOtherBot &&
        !(
          currentSession.isAngryAllowed &&
          currentSession.isAngry
        )
      ) {
        pushMessages(newMessages);
      }
    }
  }

  function checkAll() {
    if (removed) {
      return;
    }

    if (window.__ENABLED__) {
      const isEnded = chat.isChatEnded();

      if (isEnded) {
        if (isEnabled) {
          console.info('===================== END OF CHAT =====================');
          stats.onChatEnd(chat, userHash, Date.now() - chatStartTs);

          isEnabled = false;
        }

        handleEnd();

        if (window.__AUTORESET__) {
          ++chatIndex;

          const isNewChatStarted = chat.tryStartNewChat();

          if (isNewChatStarted) {
            window.enableBot();
          }
        }
      } else {
        if (isEnabled) {
          if (
            window.__AUTORESET__
            && Date.now() - window.__SESSION__.lastActivityTs > window.__RESTART_TIMEOUT__
          ) {
            chat.close();
          }
        } else {
          console.info('===================== START OF CHAT =====================');
          stats.onChatStart(chat);
          chatStartTs = Date.now();
          userHash = chat.getOpponentHash();

          isEnabled = true;
        }

        checkMessages();
      }
    }

    setTimeout(checkAll, window.__TIMEOUT__);
  }

  checkAll();
}
