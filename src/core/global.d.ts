import type {
  SessionType,
} from './types';

export {};

declare global {
  interface Window {
    __INITED__: boolean;
    __ENABLED__: boolean;
    __TIMEOUT__: number;
    __RESTART_TIMEOUT__: number;
    __AUTORESET__: boolean;
    __SESSION__: SessionType;
    enableBot: () => void;
    disableBot: () => void;
    toggleBot: () => void;
    updateBot: () => void;
  }
}
