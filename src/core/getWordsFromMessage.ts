import { distance } from 'fastest-levenshtein';

import type {
  MessageLexems,
  WordsCache,
} from './types';

const BIG_WORD_MIN_LENGTH = 4;
const LEVENSHTEIN_ERROR = 0.25;

export const getWordsFromMessage = (
  textLower: string,
  wordsCache: WordsCache,
): MessageLexems => {
  const allWords = textLower.split(/[^а-яё]+/);

  let smallWords: string[] = [];
  let bigWords: string[] = [];

  allWords.forEach((word) => {
    if (word.length >= BIG_WORD_MIN_LENGTH) {
      bigWords.push(word);
    } else {
      smallWords.push(word);
    }
  });

  const words = [];
  const adjectives = [];
  const verbs = [];
  const manNames = [];
  const womanNames = [];
  const cities = [];

  bigWords.forEach((word) => {
    wordsCache.words.forEach((wordbookWord) => {
      if (distance(word, wordbookWord) < wordbookWord.length * LEVENSHTEIN_ERROR) {
        words.push(wordsCache.wordsDict[wordbookWord]);
      }
    });

    wordsCache.adjectives.forEach((wordbookWord) => {
      if (distance(word, wordbookWord) < wordbookWord.length * LEVENSHTEIN_ERROR) {
        adjectives.push(wordsCache.adjectivesDict[wordbookWord]);
      }
    });

    wordsCache.verbs.forEach((wordbookWord) => {
      if (distance(word, wordbookWord) < wordbookWord.length * LEVENSHTEIN_ERROR) {
        verbs.push(wordsCache.verbsDict[wordbookWord]);
      }
    });

    wordsCache.manNames.forEach((wordbookWord) => {
      if (distance(word, wordbookWord) < wordbookWord.length * LEVENSHTEIN_ERROR) {
        manNames.push(wordsCache.manNamesDict[wordbookWord]);
      }
    });

    wordsCache.womanNames.forEach((wordbookWord) => {
      if (distance(word, wordbookWord) < wordbookWord.length * LEVENSHTEIN_ERROR) {
        womanNames.push(wordsCache.womanNamesDict[wordbookWord]);
      }
    });

    wordsCache.cities.forEach((wordbookWord) => {
      if (distance(word, wordbookWord) < wordbookWord.length * LEVENSHTEIN_ERROR) {
        cities.push(wordsCache.citiesDict[wordbookWord]);
      }
    });

  });

  return {
    allWords,
    smallWords,
    bigWords,
    words,
    adjectives,
    verbs,
    manNames,
    womanNames,
    cities,
  };
};
