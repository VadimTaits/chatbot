import type {
  FunctionComponent,
  ComponentChildren,
} from 'preact';
import { useCallback } from 'preact/hooks';
import styled from '@emotion/styled';

const StyledLabel = styled.label({
  cursor: 'pointer',
});

type CheckboxProps = {
  checked: boolean;
  label: ComponentChildren;
  onChange: (nextValue: boolean) => void;
};

const Checkbox: FunctionComponent<CheckboxProps> = ({
  checked,
  label,
  onChange,
}) => {
  const onInputChange = useCallback(() => {
    onChange(!checked);
  }, [
    checked,
    onChange,
  ]);

  return (
    <StyledLabel>
      <input
        type="checkbox"
        checked={checked}
        onChange={onInputChange}
      />
      {' '}
      {label}
    </StyledLabel>
  );
};

export default Checkbox;
