import type {
  FunctionComponent,
} from 'preact';
import styled from '@emotion/styled';

const StyledBlock = styled.div({
  '& + &': {
    marginTop: 5,
  },
});

const Block: FunctionComponent = ({
  children,
}) => (
  <StyledBlock>
    {children}
  </StyledBlock>
);

export default Block;
