import type {
  FunctionComponent,
} from 'preact';
import {
  useState,
  useCallback,
} from 'preact/hooks';
import format from 'date-fns/format';
import styled from '@emotion/styled';

import Block from './block';
import Checkbox from './checkbox';

const StyledWrapper = styled.div({
  boxSizing: 'border-box',
  position: 'fixed',
  zIndex: 10,
  left: 0,
  bottom: 0,
  width: '100%',
  backgroundColor: '#333',
  color: '#fff',
  opacity: 0.2,
  height: 20,
  overflow: 'hidden',
  padding: 10,
  fontSize: 12,

  '&::before': {
    content: '""',
    display: 'block',
    backgroundColor: '#333',
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%',
    height: 20,
  },

  '&:hover': {
    height: 'auto',
    maxHeight: '50vh',
    opacity: 0.95,

    '&::before': {
      content: 'none',
    },
  },
});

type ControlPanelProps = {
  enabled: boolean;
  autoreset: boolean;
};

const ControlPanel: FunctionComponent<ControlPanelProps> = ({
  enabled: enabledProp,
  autoreset: autoresetProp,
}) => {
  const [enabled, setEnabled] = useState(enabledProp);
  const [autoreset, setAutoreset] = useState(autoresetProp);
  const [lastUpdate, setLastUpdate] = useState(null);

  const toggleEnabled = useCallback((nextValue) => {
    window.__ENABLED__ = nextValue;

    setEnabled(nextValue);
  }, []);

  const toggleAutoreset = useCallback((nextValue) => {
    window.__AUTORESET__ = nextValue;

    setAutoreset(nextValue);
  }, []);

  const updateBot = useCallback(() => {
    const now = new Date();

    window.updateBot();

    setLastUpdate(format(now, 'dd.LL.yyyy HH:mm:ss'));
  }, []);

  return (
    <StyledWrapper>
      {
        lastUpdate && (
          <Block>
            Последнее обновление: {lastUpdate}
          </Block>
        )
      }

      <Block>
        <Checkbox
          checked={enabled}
          label="Enabled"
          onChange={toggleEnabled}
        />
        {' '}
        <Checkbox
          checked={autoreset}
          label="Autoreset"
          onChange={toggleAutoreset}
        />
      </Block>

      <Block>
        <button
          type="button"
          onClick={updateBot}
        >
          Обновить
        </button>
      </Block>
    </StyledWrapper>
  );
};

export default ControlPanel;
