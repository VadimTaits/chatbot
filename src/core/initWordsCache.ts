import wordsBook from './wordbooks/merge/words';
import adjectivesBook from './wordbooks/merge/adjectives';
import verbsBook from './wordbooks/merge/verbs';
import namesBook from './wordbooks/merge/names';
import citiesBook from './wordbooks/merge/cities';

import type {
  LocaleType,
  WordsCache,
} from './types';

export const initWordsCache = (locale: LocaleType): WordsCache => {
  const words: string[] = [];
  const wordsDict: Record<string, number> = {};
  const adjectives: string[] = [];
  const adjectivesDict: Record<string, number> = {};
  const verbs: string[] = [];
  const verbsDict: Record<string, number> = {};
  const cities: string[] = [];
  const citiesDict: Record<string, number> = {};

  wordsBook[locale].forEach((wordContainer, index) => {
    [0, 1].forEach((containerIndex) => {
      wordContainer[containerIndex].forEach((word) => {
        wordsDict[word] = index;
        words.push(word);
      });
    })
  }, {});

  adjectivesBook[locale].forEach((adjectiveContainer, index) => {
    [0, 1, 2, 3].forEach((containerIndex) => {
      adjectiveContainer[containerIndex].forEach((adjective) => {
        adjectivesDict[adjective] = index;
        adjectives.push(adjective);
      });
    })
  }, {});

  verbsBook[locale].forEach((verbContainer, index) => {
    ['perfect', 'continous'].forEach((verbMode) => {
      verbContainer[verbMode].forEach((verbWordsContainer) => {
        verbWordsContainer.forEach((verbWordContainer) => {
          verbWordContainer.forEach((verbWord) => {
            verbsDict[verbWord] = index;
            verbs.push(verbWord);
          });
        });
      });
    });
  }, {});

  citiesBook[locale].forEach(({
    name,
  }, index) => {
    name.forEach((cityName) => {
      const cityNameLower = cityName.toLowerCase();

      citiesDict[cityNameLower] = index;
      cities.push(cityNameLower);
    });
  }, {});

  const {
    manNamesToGroupsMap,
    manNamesLowerFlatten,
    womanNamesToGroupsMap,
    womanNamesLowerFlatten,
  } = namesBook[locale];

  return {
    words,
    wordsDict,
    adjectives,
    adjectivesDict,
    verbs,
    verbsDict,
    manNames: manNamesLowerFlatten,
    manNamesDict: manNamesToGroupsMap,
    womanNames: womanNamesLowerFlatten,
    womanNamesDict: womanNamesToGroupsMap,
    cities,
    citiesDict,
  };
};
