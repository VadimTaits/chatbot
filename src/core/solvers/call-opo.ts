// TO DO: move to thematic

import getRandomItem from 'core/utils/get-random-item';
import {
  getRandomWord,
  getRandomAdjective,
  getRandomWordWithAdjective,
} from 'core/utils/get-random-word';
import {
  getRandomSingleVerb,
  getRandomVerbWithWord,
} from 'core/utils/getRandomVerb';
import { getRandomCity } from 'core/utils/getRandomCity';
import reverseString from 'core/utils/reverse-string';
import capfirst from 'core/utils/capfirst';

const simpleAnswers = {
  en: [
    (name, age) => getRandomWordWithAdjective('en'),
    (name, age) => `where ${getRandomWordWithAdjective('en')}?`,
    (name, age) => `where all ${getRandomWordWithAdjective('en', 'i', 'multiple')}?`,
    (name, age) => `why ${getRandomWordWithAdjective('en', 'i', 'single')}?`,
    (name, age) => `what for ${getRandomWordWithAdjective('en', 'i', 'single')}?`,
    (name, age) => `not ${getRandomWordWithAdjective('en', 'i', 'single')}`,
    (name, age) => `you are ${getRandomWordWithAdjective('en', 'i', 'single')}`,
    (name, age) => `you arn't ${getRandomWordWithAdjective('en', 'i', 'single')}`,
    (name, age) => `why are you ${getRandomWordWithAdjective('en', 'i', 'single')}?`,
    (name, age) => `when ${getRandomWordWithAdjective('en', 'i', 'single')}?`,
    (name, age) => `like ${getRandomWordWithAdjective('en')}`,
    (name, age) => `there are no ${getRandomWordWithAdjective('en', 'r', 'multiple')}`,
    (name, age) => `you have a ${getRandomWordWithAdjective('en', 'i', 'single')}`,
    (name, age) => `somewhere ${getRandomWordWithAdjective('en', 'i', 'single')}`,
    (name, age) => `${getRandomWordWithAdjective('en')}-${getRandomWord('en')}`,
    (name, age) => `do you forget that you a ${getRandomWordWithAdjective('en', 'i', 'single')}?`,
  ],

  ru: [
    (name, age) => getRandomWordWithAdjective('ru'),
    (name, age) => `где ${getRandomWordWithAdjective('ru')}?`,
    (name, age) => `где все ${getRandomWordWithAdjective('ru', 'i', 'multiple')}?`,
    (name, age) => `почему ${getRandomWordWithAdjective('ru', 'i', 'single')}?`,
    (name, age) => `зачем ${getRandomWordWithAdjective('ru', 'i', 'single')}?`,
    (name, age) => `не ${getRandomWordWithAdjective('ru', 'i', 'single')}`,
    (name, age) => `ты ${getRandomWordWithAdjective('ru', 'i', 'single')}`,
    (name, age) => `ты не ${getRandomWordWithAdjective('ru', 'i', 'single')}`,
    (name, age) => `почему ты ${getRandomWordWithAdjective('ru', 'i', 'single')}?`,
    (name, age) => `а где ${getRandomWordWithAdjective('ru', 'i', 'single')}?`,
    (name, age) => `а когда ${getRandomWordWithAdjective('ru', 'i', 'single')}?`,
    (name, age) => `как ${getRandomWordWithAdjective('ru')}`,
    (name, age) => `как ${getRandomWordWithAdjective('ru', 'i', 'single')}?`,
    (name, age) => `не бывает ${getRandomWordWithAdjective('ru', 'r', 'multiple')}`,
    (name, age) => `есть ${getRandomWordWithAdjective('ru', 'i', 'single')}`,
    (name, age) => `у тебя есть ${getRandomWordWithAdjective('ru', 'i', 'single')}`,
    (name, age) => `где-то ${getRandomWordWithAdjective('ru', 'i', 'single')}`,
    (name, age) => `а где-то ${getRandomWordWithAdjective('ru', 'i', 'single')}`,
    (name, age) => `${getRandomWordWithAdjective('ru')}-${getRandomWord('ru')}`,
    (name, age) => `забыл, что ты ${getRandomWordWithAdjective('ru', 'i', 'single')}?`,
    (name, age) => `ты когда ${getRandomVerbWithWord('ru', 'perfect', 'future', 'indicative', 'you', 'm', 'single')}?`,
    (name, age) => `когда ты ${getRandomVerbWithWord('ru', 'perfect', 'future', 'indicative', 'you', 'm', 'single')}?`,
    (name, age) => `ты когда ${getRandomVerbWithWord('ru', 'perfect', 'past', 'indicative', 'you', 'm', 'single')}?`,
    (name, age) => `когда ты ${getRandomVerbWithWord('ru', 'perfect', 'past', 'indicative', 'you', 'm', 'single')}?`,
    (name, age) => `зачем ты ${getRandomVerbWithWord('ru', 'perfect', 'past', 'indicative', 'you', 'm', 'single')}?`,
    (name, age) => `где ты ${getRandomVerbWithWord('ru', 'perfect', 'past', 'indicative', 'you', 'm', 'single')}?`,
    (name, age) => getRandomVerbWithWord('ru', 'perfect', 'future', 'imperative', 'you', 'm', 'single'),
    () => `ты ${getRandomVerbWithWord('ru', 'continous', 'present', 'indicative', 'you', 'm', 'single')}?`,
    // TO DO
    (name, age) => `тебе нужно сходить в ${getRandomWordWithAdjective('ru', 'v', 'single', {
      containable: ['big'],
    })}`,
    () => `ты любишь больше ${getRandomWordWithAdjective('ru', 'v', 'multiple', {
      live: [true],
    })} или ${getRandomWordWithAdjective('ru', 'v', 'multiple', {
      live: [true],
    })}?`,
    // TO DO
    () => `съезди в ${getRandomCity('ru', 'v')}`,
    () => `в ${getRandomCity('ru', 'p')} есть ${getRandomWordWithAdjective('ru')}`,
    () => getRandomSingleVerb('ru', 'continous', 'present', 'imperative', 'you', 'm', 'single'),
    () => getRandomSingleVerb('ru', 'perfect', 'present', 'imperative', 'you', 'm', 'single'),
  ],
};

const nameAnswers = {
  en: [
    (name, age) => `${name}`,
    (name, age) => `you are ${name}`,
    (name, age) => `you aren't ${name}`,
    (name, age) => `and you are ${name}`,
    (name, age) => `do you forget that you ${name}?`,
    (name, age) => `${name}!`,
    (name, age) => `${name}!!!`,
    (name, age) => `${capfirst(reverseString(name))}`,
    (name, age) => `${capfirst(reverseString(name))}!`,
    (name, age) => `you are ${capfirst(reverseString(name))}`,
    (name, age) => `and you are ${capfirst(reverseString(name))}`,
    (name, age) => `do you know that you are ${capfirst(reverseString(name))}?`,
    (name, age) => `are you exactly ${name}?`,
    (name, age) => `why are you ${name}?`,
    (name, age) => `yoг are not ${name}`,
    (name, age) => `i know that you aren't ${name}`,
    (name, age) => `who is ${name}?`,
    (name, age) => `it is clear, and who is ${name}?`,
    (name, age) => `where is ${name}?`,
    (name, age) => `because you ${name}`,
    (name, age) => `because you ${capfirst(reverseString(name))}`,
    (name, age) => `you aren't ${name} because you ${capfirst(reverseString(name))}`,
    (name, age) => `${name} ${capfirst(reverseString(name))}`,
    (name, age) => `there are no ${name}`,
    (name, age) => `strange name ${name}`,
    (name, age) => `good name ${name}`,
    (name, age) => `${name}?`,
    (name, age) => `${name}???`,
    (name, age) => `hahaha ${name}`,
    (name, age) => `big ${name}`,
    (name, age) => `small ${name}`,
    (name, age) => `funny ${name}`,
    (name, age) => `${name} you're a ${getRandomWordWithAdjective('en', 'i', 'single')}`,
    (name, age) => `${name} why are you a ${getRandomWordWithAdjective('en', 'i', 'single')}?`,
    (name, age) => `${name}, you are such a ${getRandomWordWithAdjective('en', 'i', 'single')}`,
    (name, age) => `${name}-${getRandomWord('en', 'i', 'single')}`,
    (name, age) => `do you forget that you ${name} ${getRandomWordWithAdjective('en', 'i', 'single')}?`,
    (name, age) => `${name} it means a ${getRandomWordWithAdjective('en', 'i', 'single')}`,
    (name, age) => `${name} isn't a ${getRandomWordWithAdjective('en', 'i', 'single')}`,
    (name, age) => `${getRandomWordWithAdjective('en', 'i', 'single')} ${name}`,
    (name, age) => `if you're ${name} then you have a ${getRandomWordWithAdjective('en', 'i', 'single')}`,
    (name, age) => `if you're ${name} then you have ${getRandomWordWithAdjective('en', 'i', 'multiple')}`,
    (name, age) => `if you're ${name} then you don't have ${getRandomWordWithAdjective('en', 'r', 'any')}`,
    (name, age) => `${name} where is ${getRandomWordWithAdjective('en', 'i', 'single')}?`,
    (name, age) => `${name} where are ${getRandomWordWithAdjective('en', 'i', 'multiple')}?`,
    (name, age) => `${name} where did you miss ${getRandomWordWithAdjective('en', 'r', 'any')}?`,
  ],

  ru: [
    (name, age) => `${name}`,
    (name, age) => `ты ${name}`,
    (name, age) => `а ещё ты ${name}`,
    (name, age) => `забыл, что ты ${name}?`,
    (name, age) => `${name}!`,
    (name, age) => `${name}!!!`,
    (name, age) => `${capfirst(reverseString(name))}`,
    (name, age) => `${capfirst(reverseString(name))}!`,
    (name, age) => `ты ${capfirst(reverseString(name))}`,
    (name, age) => `а ты ${capfirst(reverseString(name))}`,
    (name, age) => `знаешь, что ты ${capfirst(reverseString(name))}?`,
    (name, age) => `ты точно ${name}?`,
    (name, age) => `почему ты ${name}?`,
    (name, age) => `ты не ${name}`,
    (name, age) => `я знаю, что ты не ${name}`,
    (name, age) => `кто такой ${name}?`,
    (name, age) => `понятно, а кто такой ${name}?`,
    (name, age) => `а где ${name}?`,
    (name, age) => `потому что ты ${name}`,
    (name, age) => `потому что ты ${capfirst(reverseString(name))}`,
    (name, age) => `ты не ${name}, ты ${capfirst(reverseString(name))}`,
    (name, age) => `${name} ${capfirst(reverseString(name))}`,
    (name, age) => `не бывает ${name}`,
    (name, age) => `${name} - это такое имя`,
    (name, age) => `странное имя ${name}`,
    (name, age) => `хорошее имя ${name}`,
    (name, age) => `какой ещё ${name}?`,
    (name, age) => `${name}?`,
    (name, age) => `${name}???`,
    (name, age) => `хахаха ${name}`,
    (name, age) => `${getRandomAdjective('ru', 'm', 'i', 'single')} ${name}`,
    (name, age) => `${name}, ты ${getRandomWordWithAdjective('ru', 'i', 'single')}`,
    (name, age) => `${name}, почему ты ${getRandomWordWithAdjective('ru', 'i', 'single')}?`,
    (name, age) => `${name}, ты такой ${getRandomWordWithAdjective('ru', 'i', 'single')}`,
    (name, age) => `${name}, ты такой ${getRandomAdjective('ru', 'm', 'i', 'single')}`,
    (name, age) => `${name}-${getRandomWord('ru', 'i', 'single')}`,
    (name, age) => `забыл, что ты ${name} ${getRandomWordWithAdjective('ru', 'i', 'single')}?`,
    (name, age) => `${name}, значит ${getRandomWordWithAdjective('ru', 'i', 'single')}`,
    (name, age) => `${name} не ${getRandomWordWithAdjective('ru', 'i', 'single')}`,
    (name, age) => `${getRandomWordWithAdjective('ru', 'i', 'single')} ${name}`,
    (name, age) => `если ты ${name}, то у тебя ${getRandomWordWithAdjective('ru', 'i', 'single')}`,
    (name, age) => `если ты ${name}, то у тебя есть ${getRandomWordWithAdjective('ru')}`,
    (name, age) => `если ты ${name}, то у тебя нет ${getRandomWordWithAdjective('ru', 'r', 'any')}`,
    (name, age) => `если ты ${name}, то ты ${getRandomSingleVerb('ru', 'continous', 'present', 'indicative', 'you', 'm', 'single')}`,
    (name, age) => `если ты ${name}, то ты ${getRandomVerbWithWord('ru', 'continous', 'present', 'indicative', 'you', 'm', 'single')}`,
    (name, age) => `если ты ${name}, то у тебя в ${getRandomWordWithAdjective('ru', 'p', 'any', {
      containable: ['small'],
    })} лежит ${getRandomWordWithAdjective('ru', 'i', 'any', {
      containable: ['none', 'small'],
    })}`,
    (name, age) => `${name}, где ${getRandomWordWithAdjective('ru')}?`,
    (name) => `${name}, зачем ты ${getRandomSingleVerb('ru', 'perfect', 'past', 'indicative', 'you', 'm', 'single')}?`,
    (name) => `${name}, зачем ты ${getRandomVerbWithWord('ru', 'perfect', 'past', 'indicative', 'you', 'm', 'single')}?`,
    (name) => `${name}, где ты ${getRandomVerbWithWord('ru', 'perfect', 'past', 'indicative', 'you', 'm', 'single')}?`,
    (name) => `${name}, ты ${getRandomVerbWithWord('ru', 'perfect', 'past', 'indicative', 'you', 'm', 'single')}?`,
    (name, age) => `${name}, ${getRandomVerbWithWord('ru', 'perfect', 'present', 'imperative', 'you', 'm', 'single')}`,
    (name, age) => `${name}, ${getRandomVerbWithWord('ru', 'continous', 'present', 'imperative', 'you', 'm', 'single')}`,
  ],
};

const ageAnswers = {
  en: [
    (name, age) => `are you ${age}?`,
    (name, age) => `are you exactly ${age}?`,
    (name, age) => `if you're ${age} then i'm ${reverseString(age.toString())}`,
    (name, age) => `are you ${reverseString(age.toString())}?`,
    (name, age) => `you're not ${age}, you're ${reverseString(age.toString())}`,
    (name, age) => `${age} hahaha`,
    (name, age) => `not ${age}`,
    (name, age) => `you're not ${age}, you're ${age - 1}`,
    (name, age) => `you're not ${age}, you're ${age + 1}`,
    (name, age) => `why do you ${age}`,
    (name, age) => `at ${age - 1} you were a ${getRandomWordWithAdjective('en', 'i', 'single')}`,
    (name, age) => `at ${age + 1} you will be a ${getRandomWordWithAdjective('en', 'i', 'single')}`,
    (name, age) => `you're not ${age} because you're ${getRandomWordWithAdjective('en', 'i', 'single')}`,
    (name, age) => `where ${age} there and the ${getRandomWordWithAdjective('en', 'i', 'single')}`,
    (name, age) => `where ${age} there and ${getRandomWordWithAdjective('en', 'i', 'multiple')}`,
    (name, age) => `${getRandomWordWithAdjective('en', 'i', 'single')}${age}`,
    (name, age) => `${age}?`,
    (name, age) => `${age}??`,
    (name, age) => `${age}???`,
    (name, age) => `you ${age} and you're ${getRandomWordWithAdjective('en', 'i', 'single')}`,
  ],

  ru: [
    (name, age) => `тебе ${age}?`,
    (name, age) => `тебе точно ${age}?`,
    (name, age) => `если тебе ${age}, то мне ${reverseString(age.toString())}`,
    (name, age) => `тебе ${reverseString(age.toString())}?`,
    (name, age) => `тебе не ${age}, тебе ${reverseString(age.toString())}`,
    (name, age) => ({
      message: `${age}`,
      emotion: 'laught',
    }),
    (name, age) => `не ${age}`,
    (name, age) => `тебе не ${age}, тебе ${age - 1}`,
    (name, age) => `тебе не ${age}, тебе ${age + 1}`,
    (name, age) => `почему тебе ${age}?`,
    (name, age) => `в ${age - 1} ты был ${getRandomWordWithAdjective('ru', 'i', 'single')}`,
    (name, age) => `в ${age + 1} ты будешь ${getRandomWordWithAdjective('ru', 'i', 'single')}`,
    (name, age) => `в ${age + 1} ты ${getRandomSingleVerb('ru', 'continous', 'future', 'indicative', 'you', 'm', 'single')}`,
    (name, age) => `в ${age + 1} ты ${getRandomVerbWithWord('ru', 'continous', 'future', 'indicative', 'you', 'm', 'single')}`,
    (name, age) => `в ${age + 1} ты ${getRandomVerbWithWord('ru', 'perfect', 'future', 'indicative', 'you', 'm', 'single')}`,
    (name, age) => `в ${age + 1} ${getRandomVerbWithWord('ru', 'perfect', 'future', 'imperative', 'you', 'm', 'single')}`,
    (name, age) => `тебе не ${age}, потому что ты ${getRandomWordWithAdjective('ru', 'i', 'single')}`,
    (name, age) => `где ${age}, там и ${getRandomWordWithAdjective('ru')}`,
    (name, age) => `${getRandomWordWithAdjective('ru', 'i', 'single')}${age}`,
    (name, age) => `${age}?`,
    (name, age) => `${age}??`,
    (name, age) => `${age}???`,
    (name, age) => `не ${age}`,
    (name, age) => `какой ещё ${age}?`,
    (name, age) => `тебе ${age}, и ты ${getRandomWordWithAdjective('ru', 'i', 'single')}`,
    (name, age) => `в ${age} начинают есть ${getRandomWordWithAdjective('ru', 'v', 'single', {
      food: ['eat'],
    })}`,
    (name, age) => `в ${age} начинают пить ${getRandomWordWithAdjective('ru', 'v', 'single', {
      food: ['drink'],
    })}`,
    (name, age) => `в ${age + 1} ты поедешь в ${getRandomCity('ru', 'v')}`,
  ],
};

const nameAndAge = {
  en: [
    (name, age) => `${name} you are ${age * 2}`,
    (name, age) => `${name} you are ${reverseString(age.toString())}`,
    (name, age) => `you're ${name} and you're ${age}`,
    (name, age) => `do you forget that you ${name} and you're ${age}?`,
    (name, age) => `you're ${name} and you're ${age}`,
    (name, age) => `you're ${age} and you're ${name}`,
    (name, age) => `you're ${name}${age}`,
    (name, age) => `you're ${name}-${age}-${getRandomWord('en', 'i', 'single')}`,
    (name, age) => `when ${name} is ${age} he is a ${getRandomWordWithAdjective('en', 'i', 'single')}`,
    (name, age) => `${name} ${getRandomWordWithAdjective('en', 'i', 'single')}, ${age} лет`,
  ],

  ru: [
    (name, age) => `${name}, тебе ${age * 2}`,
    (name, age) => `${name}, тебе ${reverseString(age.toString())}`,
    (name, age) => `ты ${name}, и тебе ${age}`,
    (name, age) => `ты ${name}, и тебе ${age} лет`,
    (name, age) => `забыл, что ты ${name}, и тебе ${age}?`,
    (name, age) => `забыл, что ты ${name}, и тебе ${age} лет?`,
    (name, age) => `а ты ${name}, и тебе ${age}`,
    (name, age) => `а ты ${name}, и тебе ${age} лет`,
    (name, age) => `тебе ${age}, и ты ${name}`,
    (name, age) => `ты ${name}${age}`,
    (name, age) => `ты ${name}-${age}-${getRandomWord('ru', 'i', 'single')}`,
    (name, age) => `когда ${name} ${age}, он ${getRandomWordWithAdjective('ru', 'i', 'single')}`,
    (name, age) => `когда ${name} ${age}, он ${getRandomSingleVerb('ru', 'continous', 'present', 'indicative', 'he', 'm', 'single')}`,
    (name, age) => `когда ${name} ${age}, он ${getRandomVerbWithWord('ru', 'continous', 'present', 'indicative', 'he', 'm', 'single')}`,
    (name, age) => `${name} ${getRandomWordWithAdjective('ru', 'i', 'single')}, ${age} лет`,
  ],
};

const allNameAnswers = {
  ru: [
    ...simpleAnswers.ru,
    ...nameAnswers.ru,
  ],

  en: [
    ...simpleAnswers.en,
    ...nameAnswers.en,
  ],
};

const allAgeAnswers = {
  ru: [
    ...simpleAnswers.ru,
    ...ageAnswers.ru,
  ],
 
  en: [
    ...simpleAnswers.en,
    ...ageAnswers.en,
  ],
};

const allAnswers = {
  ru: [
    ...simpleAnswers.ru,
    ...nameAnswers.ru,
    ...ageAnswers.ru,
    ...nameAndAge.ru,
  ],

  en: [
    ...simpleAnswers.en,
    ...nameAnswers.en,
    ...ageAnswers.en,
    ...nameAndAge.en,
  ],
};

export default {
  check: (textLower, currentSession) => {
    return true;
    if (currentSession.opponent.age && currentSession.opponent.name) {
      return Math.random() < 0.3;
    }

    if (currentSession.opponent.name) {
      return Math.random() < 0.25;
    }

    if (currentSession.opponent.age) {
      return Math.random() < 0.15;
    }

    return Math.random() < 0.10;
  },

  canRepeat: true,

  answer: (textLower, currentSession) => {
    let answers;
    if (currentSession.opponent.age && currentSession.opponent.name) {
      answers = allAnswers;
    } else if (currentSession.opponent.name) {
      answers = allNameAnswers;
    } else if (currentSession.opponent.age) {
      answers = allAgeAnswers;
    } else {
      answers = simpleAnswers;
    }

    const answer = getRandomItem(answers[currentSession.locale]);

    return answer(
      currentSession.opponent.name ? getRandomItem(currentSession.opponent.name) : null,
      currentSession.opponent.age,
    );
  },

  // TO DO: remove copy-paste after move to thematic
  sendOwnMessage: (send, currentSession) => {
    let answers;
    if (currentSession.opponent.age && currentSession.opponent.name) {
      answers = allAnswers;
    } else if (currentSession.opponent.name) {
      answers = allNameAnswers;
    } else if (currentSession.opponent.age) {
      answers = allAgeAnswers;
    } else {
      answers = simpleAnswers;
    }

    const answer = getRandomItem(answers[currentSession.locale]);

    send(answer(
      currentSession.opponent.name ? getRandomItem(currentSession.opponent.name) : null,
      currentSession.opponent.age,
    ));
  },
};
