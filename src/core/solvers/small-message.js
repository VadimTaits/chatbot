import getRandomItem from 'core/utils/get-random-item';

function isSmallMessage(textLower, currentSession) {
  if (currentSession.debug) {
    return false;
  }

  if (currentSession.lastMessage === textLower) {
    return true;
  } else {
    currentSession.lastMessage = textLower;
  }

  return textLower.length < 3;
}

const asnswers = {
  en: [
    'a',
    'b',
    'c',
    'd',
    'e',
    'f',
    'g',
    'h',
    'i',
    'j',
    'k',
    'l',
    'm',
    'n',
    'o',
    'p',
    'q',
    'r',
    's',
    't',
    'u',
    'v',
    'w',
    'x',
    'y',
    'z',
  ],

  ru: [
    'а',
    'б',
    'в',
    'г',
    'д',
    'е',
    'ё',
    'ж',
    'з',
    'и',
    'й',
    'к',
    'л',
    'м',
    'н',
    'о',
    'п',
    'р',
    'с',
    'т',
    'у',
    'ф',
    'х',
    'ц',
    'ч',
    'ш',
    'щ',
    'ъ',
    'ы',
    'ь',
    'э',
    'ю',
    'я',
  ],
};

function sendSmallAnswer(textLower, currentSession) {
  const rnd = Math.random();

  if (rnd > 0.2) {
    if (rnd > 0.8) {
      return `${getRandomItem(asnswers[currentSession.locale])}${getRandomItem(asnswers[currentSession.locale])}`;
    }

    return getRandomItem(asnswers[currentSession.locale]);
  }

  return null;
}

export default {
  check: isSmallMessage,

  canRepeat: true,

  answer: sendSmallAnswer,
};
