import { getRandomWordWithAdjective } from 'core/utils/get-random-word';

const re = {
  ru: /[а-яё ]+/gim,
};

function classifyMixedWord(textLower, currentSession) {
  if (Math.random() > 0.2) {
    return null;
  }

  const matchedStr = textLower.match(re[currentSession.locale]);
  
  if (!matchedStr) {
    return null;
  }
  
  const processedStr = matchedStr[0].trim();
  
  if (processedStr.includes(' ') || processedStr.length < 4) {
    return null;
  }
  
  return processedStr;
}

function sendMixedWordAnswer(textLower, currentSession, processedWord) {
  const randomWord = getRandomWordWithAdjective(currentSession.locale);
  const randomWordHalfLength = randomWord.length / 2;
  
  const firstWordLength = Math.min(
    randomWordHalfLength + Math.random() * randomWordHalfLength,
    randomWord.length - 2,
  );
  
  const processedWordLength = Math.max(
    Math.random() * processedWord.length / 2,
    2,
  );
  
  const firstSubstr = randomWord.substring(0, firstWordLength);
  const secondSubstr = processedWord.substring(
    processedWord.length - processedWordLength,
    processedWord.length,
  );

  return `${firstSubstr}${secondSubstr}`;
}

export default {
  check: classifyMixedWord,
  canRepeat: true,
  answer: sendMixedWordAnswer,
};
