import getRandomItem from 'core/utils/get-random-item';

import wordsBook from 'core/wordbooks/merge/words';
import adjectivesBook from 'core/wordbooks/merge/adjectives';
import verbsBook from 'core/wordbooks/merge/verbs';
import namesBook from 'core/wordbooks/merge/names';
import citiesBook from 'core/wordbooks/merge/cities';

import {
  pickWordFromContainer,
  getRandomAdjectiveWithSelectedWord,
  getRandomWordWithAdjective,
  pickAdjectiveFromContainer,
} from 'core/utils/get-random-word';

import {
  pickCityName,
} from 'core/utils/getRandomCity';

import {
  getRandomSingleVerb,
  getVerbString,
  getRandomVerbWithWord,
  getCurrentVerbWithRandomWord,
} from 'core/utils/getRandomVerb';

import type {
  AdjectiveType,
  CityType,
  FullSendMessageType,
  LocaleType,
  SessionType,
  SolverAnswerType,
  SolverCheckType,
  SolverType,
  WordType,
  VerbType,
} from 'core/types';

type CollectedStats = {
  words: number[];
  adjectives: number[];
  verbs: number[];
  manNames: number[];
  womanNames: number[];
  cities: number[];
};

const getStats = ({
  parsedLastMessages,
}: SessionType): CollectedStats => {
  let words: number[] = [];
  let adjectives: number[] = [];
  let verbs: number[] = [];
  let manNames: number[] = [];
  let womanNames: number[] = [];
  let cities: number[] = [];

  parsedLastMessages.forEach((parsedMessage) => {
    words = [...words, ...parsedMessage.words];
    adjectives = [...adjectives, ...parsedMessage.adjectives];
    verbs = [...verbs, ...parsedMessage.verbs];
    manNames = [...manNames, ...parsedMessage.manNames];
    womanNames = [...womanNames, ...parsedMessage.womanNames];
    cities = [...cities, ...parsedMessage.cities];
  });

  if (
    words.length === 0
    && adjectives.length === 0
    && verbs.length === 0
    && manNames.length === 0
    && womanNames.length === 0
    && cities.length === 0
  ) {
    return null;
  }

  return {
    words,
    adjectives,
    verbs,
    manNames,
    womanNames,
    cities,
  };
};

const check: SolverCheckType<CollectedStats> = (textLower, session) => {
  if (Math.random() > 0.5) {
    return null;
  }

  return getStats(session);
};

type AnswerItemType = {
  message: (params: {
    word?: WordType;
    adjective?: AdjectiveType;
    verb?: VerbType;
    manName: string[];
    womanName: string[];
    city?: CityType;
  }) => FullSendMessageType;

  deps?: {
    word?: boolean;
    adjective?: boolean;
    verb?: boolean;
    manName?: boolean;
    womanName?: boolean;
    city?: boolean;
  };
};

const answers: Record<LocaleType, AnswerItemType[]> = {
  en: [],

  ru: [
    // word

    {
      message: ({
        word,
      }) => `${pickWordFromContainer(word, 'ru', 'i', 'single')}?`,

      deps: {
        word: true,
      },
    },

    {
      message: ({
        word,
      }) => `почему ${pickWordFromContainer(word, 'ru', 'i', 'any')}?`,

      deps: {
        word: true,
      },
    },

    {
      message: ({
        word,
      }) => `причём тут ${pickWordFromContainer(word, 'ru', 'i', 'any')}?`,

      deps: {
        word: true,
      },
    },

    {
      message: ({
        word,
      }) => `${getRandomAdjectiveWithSelectedWord('ru', word, 'i', 'any')}?`,

      deps: {
        word: true,
      },
    },

    // adjective

    {
      message: ({
        adjective,
      }) => `ты ${pickAdjectiveFromContainer('ru', adjective, 'm', 'i', 'any')}?`,

      deps: {
        adjective: true,
      },
    },

    {
      message: ({
        adjective,
      }) => `сам ты ${pickAdjectiveFromContainer('ru', adjective, 'm', 'i', 'any')}`,

      deps: {
        adjective: true,
      },
    },

    {
      message: ({
        adjective,
      }) => `не ${pickAdjectiveFromContainer('ru', adjective, 'm', 'i', 'any')}`,

      deps: {
        adjective: true,
      },
    },

    {
      message: ({
        adjective,
      }) => `не ${pickAdjectiveFromContainer('ru', adjective, 'm', 'i', 'any')}`,

      deps: {
        adjective: true,
      },
    },

    {
      message: ({
        adjective,
        word,
      }) => `${pickAdjectiveFromContainer('ru', adjective, word[2].sex, 'i', 'single')} ${pickWordFromContainer(word, 'ru', 'i', 'single')}`,

      deps: {
        adjective: true,
        word: true,
      },
    },

    {
      message: ({
        adjective,
        word,
      }) => `${pickWordFromContainer(word, 'ru', 'i', 'single')} не ${pickAdjectiveFromContainer('ru', adjective, word[2].sex, 'i', 'single')}`,

      deps: {
        adjective: true,
        word: true,
      },
    },

    // city

    {
      message: ({
        city,
      }) => `в ${pickCityName(city, 'ru', 'p')} есть ${getRandomWordWithAdjective('ru')}`,

      deps: {
        city: true,
      },
    },

    {
      message: ({
        city,
      }) => `в ${pickCityName(city, 'ru', 'p')} нет ${getRandomWordWithAdjective('ru', 'r')}`,

      deps: {
        city: true,
      },
    },

    {
      message: ({
        city,
      }) => `а где ${pickCityName(city, 'ru', 'p')}?`,

      deps: {
        city: true,
      },
    },

    {
      message: ({
        city,
      }) => `кто живёт ${pickCityName(city, 'ru', 'p')}?`,

      deps: {
        city: true,
      },
    },

    {
      message: ({
        city,
        word,
      }) => `в ${pickCityName(city, 'ru', 'p')} есть ${pickWordFromContainer(word, 'ru')}?`,

      deps: {
        city: true,
        word: true,
      },
    },

    {
      message: ({
        city,
        word,
      }) => `в ${pickCityName(city, 'ru', 'p')} нет ${pickWordFromContainer(word, 'ru', 'r')}?`,

      deps: {
        city: true,
        word: true,
      },
    },

    {
      message: ({
        city,
        adjective,
      }) => `${pickAdjectiveFromContainer('ru', adjective, city.sex, 'i', 'any')} ${pickCityName(city, 'ru', 'i')}`,

      deps: {
        city: true,
        adjective: true,
      },
    },

    // manName

    {
      message: ({
        manName,
      }) => `кто такой ${getRandomItem(manName)}?`,

      deps: {
        manName: true,
      },
    },

    {
      message: ({
        manName,
      }) => ({
        message: getRandomItem(manName),
        emotion: 'laught',
      }),

      deps: {
        manName: true,
      },
    },

    {
      message: ({
        manName,
      }) => `${getRandomItem(manName)} это ${getRandomItem(manName)}`,

      deps: {
        manName: true,
      },
    },

    {
      message: ({
        manName,
      }) => `${getRandomItem(manName)} ${getRandomSingleVerb('ru', 'continous', 'present', 'indicative', 'he', 'm', 'single')}`,

      deps: {
        manName: true,
      },
    },


    {
      message: ({
        manName,
      }) => `${getRandomItem(manName)} ${getRandomSingleVerb('ru', 'perfect', 'past', 'indicative', 'he', 'm', 'single')}`,

      deps: {
        manName: true,
      },
    },


    {
      message: ({
        manName,
      }) => `${getRandomItem(manName)} ${getRandomVerbWithWord('ru', 'continous', 'present', 'indicative', 'he', 'm', 'single')}`,

      deps: {
        manName: true,
      },
    },


    {
      message: ({
        manName,
      }) => `${getRandomItem(manName)} ${getRandomVerbWithWord('ru', 'perfect', 'past', 'indicative', 'he', 'm', 'single')}`,

      deps: {
        manName: true,
      },
    },

    {
      message: ({
        adjective,
        manName,
      }) => `${pickAdjectiveFromContainer('ru', adjective, 'm', 'i', 'single')} ${getRandomItem(manName)}`,

      deps: {
        manName: true,
        adjective: true,
      },
    },

    {
      message: ({
        adjective,
        manName,
      }) => `${getRandomItem(manName)} не ${pickAdjectiveFromContainer('ru', adjective, 'm', 'i', 'single')}`,

      deps: {
        manName: true,
        adjective: true,
      },
    },

    {
      message: ({
        adjective,
        manName,
      }) => ({
        message: `почему ${getRandomItem(manName)} ${pickAdjectiveFromContainer('ru', adjective, 'm', 'i', 'single')}?`,
        emotion: 'not_understand',
      }),

      deps: {
        manName: true,
        adjective: true,
      },
    },

    {
      message: ({
        word,
        manName,
      }) => ({
        message: `${getRandomItem(manName)} ${pickWordFromContainer(word, 'ru', 'i', 'single')}`,
        emotion: 'laught',
      }),

      deps: {
        manName: true,
        word: true,
      },
    },

    {
      message: ({
        word,
        manName,
      }) => `${getRandomItem(manName)} не ${pickWordFromContainer(word, 'ru', 'i', 'single')}`,

      deps: {
        manName: true,
        word: true,
      },
    },

    {
      message: ({
        word,
        manName,
      }) => ({
        message: `почему ${getRandomItem(manName)} ${pickWordFromContainer(word, 'ru', 'i', 'single')}?`,
        emotion: 'not_understand',
      }),

      deps: {
        manName: true,
        word: true,
      },
    },

    {
      message: ({
        verb,
        manName,
      }) => `${getRandomItem(manName)} ${getVerbString('ru', verb, 'continous', 'present', 'indicative', 'he', 'm', 'single')}`,

      deps: {
        verb: true,
        manName: true,
      },
    },


    {
      message: ({
        verb,
        manName,
      }) => `${getRandomItem(manName)} ${getVerbString('ru', verb, 'perfect', 'past', 'indicative', 'he', 'm', 'single')}`,

      deps: {
        verb: true,
        manName: true,
      },
    },


    {
      message: ({
        verb,
        manName,
      }) => `${getRandomItem(manName)} ${getCurrentVerbWithRandomWord(verb, 'ru', 'continous', 'present', 'indicative', 'he', 'm', 'single')}`,

      deps: {
        verb: true,
        manName: true,
      },
    },

    {
      message: ({
        verb,
        manName,
      }) => `${getRandomItem(manName)} ${getCurrentVerbWithRandomWord(verb, 'ru', 'perfect', 'past', 'indicative', 'he', 'm', 'single')}`,

      deps: {
        verb: true,
        manName: true,
      },
    },

    // womanName

    {
      message: ({
        womanName,
      }) => ({
        message: `кто такая ${getRandomItem(womanName)}?`,
        emotion: 'not_understand',
      }),

      deps: {
        womanName: true,
      },
    },

    {
      message: ({
        womanName,
      }) => ({
        message: getRandomItem(womanName),
        emotion: 'laught',
      }),

      deps: {
        womanName: true,
      },
    },

    {
      message: ({
        womanName,
      }) => `${getRandomItem(womanName)} это ${getRandomItem(womanName)}`,

      deps: {
        womanName: true,
      },
    },

    {
      message: ({
        womanName,
      }) => `${getRandomItem(womanName)} ${getRandomSingleVerb('ru', 'continous', 'present', 'indicative', 'she', 'f', 'single')}`,

      deps: {
        womanName: true,
      },
    },


    {
      message: ({
        womanName,
      }) => `${getRandomItem(womanName)} ${getRandomSingleVerb('ru', 'perfect', 'past', 'indicative', 'she', 'f', 'single')}`,

      deps: {
        womanName: true,
      },
    },


    {
      message: ({
        womanName,
      }) => `${getRandomItem(womanName)} ${getRandomVerbWithWord('ru', 'continous', 'present', 'indicative', 'she', 'f', 'single')}`,

      deps: {
        womanName: true,
      },
    },


    {
      message: ({
        womanName,
      }) => `${getRandomItem(womanName)} ${getRandomVerbWithWord('ru', 'perfect', 'past', 'indicative', 'she', 'f', 'single')}`,

      deps: {
        womanName: true,
      },
    },

    {
      message: ({
        adjective,
        womanName,
      }) => `${pickAdjectiveFromContainer('ru', adjective, 'f', 'i', 'single')} ${getRandomItem(womanName)}`,

      deps: {
        womanName: true,
        adjective: true,
      },
    },

    {
      message: ({
        adjective,
        womanName,
      }) => `${getRandomItem(womanName)} не ${pickAdjectiveFromContainer('ru', adjective, 'f', 'i', 'single')}`,

      deps: {
        womanName: true,
        adjective: true,
      },
    },

    {
      message: ({
        adjective,
        womanName,
      }) => ({
        message: `почему ${getRandomItem(womanName)} ${pickAdjectiveFromContainer('ru', adjective, 'f', 'i', 'single')}?`,
        emotion: 'not_understand',
      }),

      deps: {
        womanName: true,
        adjective: true,
      },
    },

    {
      message: ({
        word,
        womanName,
      }) => ({
        message: `${getRandomItem(womanName)} ${pickWordFromContainer(word, 'ru', 'i', 'single')}`,
        emotion: 'laught',
      }),

      deps: {
        womanName: true,
        word: true,
      },
    },

    {
      message: ({
        word,
        womanName,
      }) => `${getRandomItem(womanName)} не ${pickWordFromContainer(word, 'ru', 'i', 'single')}`,

      deps: {
        womanName: true,
        word: true,
      },
    },

    {
      message: ({
        word,
        womanName,
      }) => ({
        message: `почему ${getRandomItem(womanName)} ${pickWordFromContainer(word, 'ru', 'i', 'single')}?`,
        emotion: 'not_understand',
      }),

      deps: {
        womanName: true,
        word: true,
      },
    },

    {
      message: ({
        verb,
        womanName,
      }) => `${getRandomItem(womanName)} ${getVerbString('ru', verb, 'continous', 'present', 'indicative', 'she', 'f', 'single')}`,

      deps: {
        verb: true,
        womanName: true,
      },
    },


    {
      message: ({
        verb,
        womanName,
      }) => `${getRandomItem(womanName)} ${getVerbString('ru', verb, 'perfect', 'past', 'indicative', 'she', 'f', 'single')}`,

      deps: {
        verb: true,
        womanName: true,
      },
    },


    {
      message: ({
        verb,
        womanName,
      }) => `${getRandomItem(womanName)} ${getCurrentVerbWithRandomWord(verb, 'ru', 'continous', 'present', 'indicative', 'she', 'f', 'single')}`,

      deps: {
        verb: true,
        womanName: true,
      },
    },

    {
      message: ({
        verb,
        womanName,
      }) => `${getRandomItem(womanName)} ${getCurrentVerbWithRandomWord(verb, 'ru', 'perfect', 'past', 'indicative', 'she', 'f', 'single')}`,

      deps: {
        verb: true,
        womanName: true,
      },
    },

    // verb

    {
      message: ({
        verb,
      }) => `я ${getCurrentVerbWithRandomWord(verb, 'ru', 'perfect', 'present', 'indicative', 'i', 'f', 'single')}`,

      deps: {
        verb: true,
      },
    },

    {
      message: ({
        verb,
      }) => `я не ${getCurrentVerbWithRandomWord(verb, 'ru', 'perfect', 'present', 'indicative', 'i', 'f', 'single')}`,

      deps: {
        verb: true,
      },
    },

    {
      message: ({
        verb,
      }) => `ты ${getCurrentVerbWithRandomWord(verb, 'ru', 'perfect', 'present', 'indicative', 'you', 'm', 'single')}?`,

      deps: {
        verb: true,
      },
    },

    {
      message: ({
        verb,
      }) => `ты ${getVerbString('ru', verb, 'continous', 'present', 'indicative', 'you', 'm', 'single')}?`,

      deps: {
        verb: true,
      },
    },

    {
      message: ({
        verb,
      }) => `сам ${getVerbString('ru', verb, 'perfect', 'present', 'imperative', 'you', 'm', 'single')}`,

      deps: {
        verb: true,
      },
    },

    {
      message: ({
        verb,
      }) => `зачем ты ${getVerbString('ru', verb, 'perfect', 'past', 'indicative', 'you', 'm', 'single')}?`,

      deps: {
        verb: true,
      },
    },

    {
      message: ({
        verb,
      }) => `зачем ты ${getCurrentVerbWithRandomWord(verb, 'ru', 'perfect', 'past', 'indicative', 'you', 'm', 'single')}?`,

      deps: {
        verb: true,
      },
    },
  ],
};

const getMessage = (
  {
    locale,
  }: SessionType,
  {
    words,
    adjectives,
    verbs,
    manNames,
    womanNames,
    cities,
  }: CollectedStats,
): FullSendMessageType => {
  const word = words.length > 0
  ? wordsBook[locale][getRandomItem(words)]
  : null;
  const adjective = adjectives.length > 0
    ? adjectivesBook[locale][getRandomItem(adjectives)]
    : null;
  const verb = verbs.length > 0
    ? verbsBook[locale][getRandomItem(verbs)]
    : null;
  const manName = manNames.length > 0
    ? namesBook[locale].manNames[getRandomItem(manNames)]
    : null;
  const womanName = womanNames.length > 0
    ? namesBook[locale].womanNames[getRandomItem(womanNames)]
    : null;
  const city = cities.length > 0
    ? citiesBook[locale][getRandomItem(cities)]
    : null;

  const filteredAnswers = answers[locale].filter(({
    deps,
  }) => {
    if (!deps) {
      return true;
    }

    if (deps.word && !word) {
      return false;
    }

    if (deps.adjective && !adjective) {
      return false;
    }

    if (deps.verb && !verb) {
      return false;
    }

    if (deps.manName && !manName) {
      return false;
    }

    if (deps.womanName && !womanName) {
      return false;
    }

    if (deps.city && !city) {
      return false;
    }

    return true;
  });

  const answerForSend = getRandomItem(filteredAnswers);

  return answerForSend.message({
    word,
    adjective,
    verb,
    manName,
    womanName,
    city,
  });
};

const answer: SolverAnswerType<CollectedStats> = (
  textLower,
  session,
  stats,
) => getMessage(session, stats);

export const thematic: SolverType<CollectedStats> = {
  check,
  answer,

  sendOwnMessage: (send, session) => {
    if (Math.random() > 0.5) {
      return;
    }

    const stats = getStats(session);

    if (!stats) {
      return;
    }

    send(getMessage(session, stats));
  },
};
