import { activity } from './activity';
import { age } from './age';
import angry from './angry';
import anyTab from './any-tab';
import bot from './bot';
import bye from './bye';
import call from './call';
import callOpo from './call-opo';
import city from './city';
import conundrums from './conundrums';
import { hello } from './hello';
import manyWords from './manyWords';
import mixedWord from './mixed-word';
import name from './name';
import nicely from './nicely';
import noYou from './no-you';
import otherBot from './other-bot';
import ownInfo from './own-info';
import proof from './proof';
import { randomQuestion } from './random-question';
import repeatWord from './repeat-word';
import reservedSmallMessage from './reserved-small-message';
import relationship from './relationship';
import stupid from './stupid';
import sex from './sex';
import shuffleMessage from './shuffle-message';
import smallMessage from './small-message';
import { thematic } from './thematic';
import { questions } from './questions';

import randomWord from './random-word';

import type {
  SolverType,
} from '../types';

export const solvers: SolverType<any>[] = [
  otherBot,

  angry,

  activity,
  age,
  bot,

  proof,

  noYou,
  call,
  city,
  hello,
  name,
  relationship,
  sex,
  stupid,
  nicely,

  questions,
  conundrums,

  reservedSmallMessage,
  smallMessage,

  manyWords,
  bye,

  mixedWord,
  shuffleMessage,
  repeatWord,
  randomQuestion,

  anyTab,

  ownInfo,
  thematic,
  callOpo,

  randomWord,
];
