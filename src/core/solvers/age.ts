import getRandomItem from '../utils/get-random-item';
import type {
  LocaleType,
  FullSendMessageType,
  SolverCheckType,
  SolverAnswerType,
  SolverType,
} from '../types';

const ageRe = /\d+/gim;

type AgePayload = 'question' | 'own_age';

const isAgeDetectQuestion: SolverCheckType<AgePayload> = (textLower, currentSession) => {
  if (!currentSession.opponent.age) {
    const matches = textLower.match(ageRe);

    if (matches && matches.length > 0 && matches[0].length === 2) {
      currentSession.opponent.age = parseInt(matches[0], 10);

      return 'own_age';
    }
  }
  
  switch (currentSession.locale) {
    case 'en':
      if (
        textLower.includes('age') ||
        (
          textLower.indexOf('how') === 0 && textLower.includes('old')
        )
      ) {
        return 'question';
      };

    case 'ru':
      if (
        (
          (
            textLower.includes('лет') ||
            textLower.includes('тебе') ||
            textLower.includes('годиков') ||
            textLower.includes('вам')
          ) && (
            textLower.includes('ск ') ||
            textLower.includes('скок') ||
            textLower.includes('скольк') ||
            textLower.includes('сколко')
          )
        ) || (
          textLower.includes('возраст')
        ) || (
          textLower.indexOf('лет') === 0 && textLower.includes('?')
        ) || (
          textLower.includes('взрослая') && textLower.includes('?')
        ) || (
          textLower.includes('ск те')
        ) || (
          textLower === 'лет'
        )
      ) {
        return 'question';
      };

    default:
      return null;
  }
}

type AgeAnswer = (age: number) => FullSendMessageType;

const answers: Record<LocaleType, AgeAnswer[]> = {
  en: [
    (age) => `${age}`,
  ],

  ru: [
    (age) => `${age}`,
    (age) => `${age} же`,
    (age) => `я говорила, что ${age}`,
    (age) => `я уже говорила, что ${age}`,
    (age) => `не помнишь, что ${age}`,
    (age) => `я же уже говорила, ${age}`,
  ],
};

const questions: Record<LocaleType, FullSendMessageType[]> = {
  en: [
    'how old?',
  ],

  ru: [
    'сколько лет?',
    'а сколько лет?',
    'а лет сколько?',
    'а сколько тебе лет?',
    'и сколько тебе лет?',
    'и сколько же тебе лет?',
    'сколько же тебе лет?',
    {
      message: 'признавайся, сколько тебе лет',
      emotion: 'laught',
    },
    {
      message: 'интересно, сколько тебе лет?',
      emotion: 'not_understand',
    },
    {
      message: 'даже не знаю, сколько тебе лет',
      emotion: 'not_understand',
    },
  ],
};

const sendAgeAnswer: SolverAnswerType<AgePayload> = (textLower, currentSession) => {
  if (!currentSession.solversPayload.age) {
    currentSession.solversPayload.age = {};
  }

  if (!currentSession.solversPayload.age.sent) {
    currentSession.solversPayload.age.sent = true;
  
    return currentSession.age.toString();
  }

  return getRandomItem(answers[currentSession.locale])(currentSession.age);
}

export const age: SolverType<AgePayload> = {
  check: isAgeDetectQuestion,
  canRepeat: false,
  answer: sendAgeAnswer,

  sendOwnMessage: (send, currentSession) => {
    if (currentSession.opponent.age || !currentSession.solversPayload.hello?.sent) {
      return;
    }

    if (Math.random() > 0.5) {
      return;
    }

    send(getRandomItem(questions[currentSession.locale]));
  },
};
