const phoneRe = /[^\d]/gim;

const otherBotMessages = {
  en: [
    [
      'marrypuss',
      'derion.ga',
      'qps.ru',
      'waitforu.ga',
    ],
  ],

  ru: [
    [
      'fiagro',
      'olyaatihonova',
      'craypierrot',
      'qetina',
      'talasil',
      'webgirlls',
      'relaxmasha',
      'tanyaxz',
      'Liza_XX',
      'vikysyaa',
      'viktoriaphoto',
      'etochat',
      'mashakras',
      'лера гранова',
      'uitru',
      '.do.am',
      'usex.ml',
      'syka.ml',
      'xatora',
      'xanota',
      'xahota',
      'kotrana',
      'kotxana',
      'svetamitty',
      'sexyclick.info',
      'оллятих',
      'dima23demon',
      's​​na​​pse​​x',
      'олятихомирова',
      'tе-гpam',
      'ле︊︊рка︊б︊︊и︊︊.︊︊р︊︊ф',
      'л︊е︊р︊ка︊б︊︊и︊︊.︊︊р︊︊︊ф',
      'Нелефау',
    ],
    [
      '20шт 300р',
    ],
    [
      'денис никитенко',
    ],
    [
      'тебе не надоело тут с ботами общаться',
    ],
    [
      'ищу такого же задрота и ноулайфера',
    ],
    [
      'нужна 13 летняя',
    ],
    [
      'ну ты и хуйню написал',
      'что ты там лепечешь',
      'ну ты и долбоёб',
      'надо пизду зашить',
      'ты хохлорусня, что ли',
      'твой папашка в твою попашку',
      'в школе физрук в мозг',
      'жертва аурикулярного насилия',
      'ну и ссыкливое же ты ничтожество',
    ],
    [
      'я понял почему ты ебанутый',
      'мне вылежишь анус от говна',
      'бабушку твою выебал',
      'сука понос ебаный',
      'твоей жопу рвал своей',
      'ты заднеприводный петушара',
      'на стену кончал',
      'пизду своих одноклассниц',
      'жопа неебаная',
      'пизды бля выползи',
      'вагину хотя бы видел',
      'захлебнись спермой',
      'набитым хуями ртом',
      'хуй засуну в очко',
      'proxxx70',
      'смотрела и ласкалась',
      'страпоном в жопу',
      'правую руку ебеш',
      'пиздиш невнятно',
      'телефон дай мне сотов',
      'у тебя коротковат',
      'сотовый дай я звякну',
      'пизду пидорасил',
      'я тебя вычислю',
      'твою маму всегда вижу',
      'я тебя инвалидом',
      'сука выключи не беси',
      'маму трахну',
      'тебе сломаю ослоеб',
      'дрочить тебе буду',
      'мне жопу лизал',
      'засыш со мной поговорить',
      'срал из жопы',
      'хуйцами немытыми',
      'ссал на твой гроб',
      'ссал тебе',
      'срал тебе',
      'омлет из твоих яиц',
      'кажется у тебя член в очке',
      'пидорасы на этом чате сидят и ты тоже',
      'слизывал с твоей мамы',
      'хуй давно ждет ласки',
      'тебя выебу тебе даже срать больно',
      'прямо как ты на моем хуе',
      'вчера видел как твоя мама',
      'нахер дерзкий бык дерзкий бля',
      'маме твоей очко сверлил',
      'хер к стене прикалачю',
      'мамаша ебаца любит',
      'когда аноним нехуево смелый',
      'мастер спорта по ебле',
      'в жопе твоей мамки',
      'ты бля про очко только говориш',
      'за базар ты не в ответе чтоли',
      'твоей маме в жопу',
      'на член ко мне присядь',
      'далбаеб обиженый лох',
      'за манитором хули не пиздеть',
      'гомосня одни жопы и анусы',
    ],
  ],
};

export default {
  check: (textLower, currentSession) => {
    if (currentSession.messagesCount > 20) {
      return false;
    }

    return textLower.split(phoneRe).join('').length > 10 ||
      otherBotMessages[currentSession.locale]
        .some((messagesArray) =>
          messagesArray.some((message) =>
            textLower.includes(message)));

    if (isOtherBot) {
      currentSession.isOtherBot = true;
      return true;
    }

    return false;
  },

  tryRepeat: () => true,
  canRepeat: true,

  answer: (textLower, currentSession) => {
    switch (currentSession.locale) {
      case 'ru':
        return 'ты бот';

      default:
        return 'you\'re bot';
    }
  },
};
