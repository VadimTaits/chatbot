import getRandomItem from 'core/utils/get-random-item';

function classifyProofQuestion(textLower, currentSession) {
  switch (currentSession.locale) {
    case 'ru':
      if (
        textLower.includes('докажи') ||
        textLower.includes('докажешь')
      ) {
        return 'proof';
      }

      if (
        textLower.includes('покажи') ||
        textLower.includes('сфоткай') ||
        textLower.includes('фотку') ||
        textLower.includes('давай по фотке') ||
        textLower.includes('фоточку') ||
        textLower.includes('cкинь фото')
      ) {
        return 'show';
      }

      return null;

    default:
      return false;
  }
}

const proofAnswers = {
  'ru': {
    'show': [
      'кого?',
      'себя чтоли?',
      'себя?',
      'тебя?',
      'что показать?',
      'темно',
      'телефон без камеры',
      'не умею',
      'не зарегистрирована',
      'не хочу',
      'зачем?',
      'кому?',
      'пффф',
      'ещё чего?',
      'как?',
      'каким образом?',
      'почему я должна что-то показывать?',
      'нет, ты',
      'уже показала',
      'показала',
      'телефон сел',
      'интернет платный',
      'у меня интернет платный',
    ],

    'proof': [
      'аксиома',
      'элементарно',
      'по теореме',
      'сам докажи',
      'ты докажи',
      'не хочу',
      'зачем?',
      'кому?',
      'пффф',
      'ещё чего?',
      'как?',
      'каким образом?',
      'почему я должна что-то доказывать?',
      'нет, ты',
      'это все знают',
      'это экзамен чтоли?',
      'уже доказала',
      'доказала',
    ],
  },
};

function sendProofAnswer(textLower, currentSession, classifier) {
  return getRandomItem(proofAnswers[currentSession.locale][classifier]);
}

export default {
  check: classifyProofQuestion,
  canRepeat: true,
  answer: sendProofAnswer,
};
