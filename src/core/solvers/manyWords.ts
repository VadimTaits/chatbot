import getRandomItem from 'core/utils/get-random-item';

import type {
  LocaleType,
  SolverCheckType,
  SolverAnswerType,
  FullSendMessageType,
} from 'core/types';

const isManyWordsQuestion: SolverCheckType<boolean> = (
  textLower,
  {
    parsedLastMessages,
  },
) => {
  if (Math.random() > 0.8) {
    return false;
  }

  if (parsedLastMessages.length === 0) {
    return false;
  }

  return parsedLastMessages[parsedLastMessages.length - 1].allWords.length > 5;
};

const manyWordsMessages: Record<LocaleType, FullSendMessageType[]> = {
  ru: [
    'вау',
    'ого',
    'ничего себе',
    'интересненько',
    'не может быть',
    'ничего страшного',
    'как же так?',
    'прикольно',
    'прикольненько',
    'нормально так',
    'норм',
    {
      message: 'норм',
      emotion: 'laught',
    },
    'бывает',
    'бывает такое',
    'ну да, бывает',
    'не может быть',
    'а что потом?',
    'а потом?',
    'а дальше?',
    'и что это значит?',
    'и что думаешь?',
    'почему же?',
    'почему так?',
    'а почему?',
  ],
  
  en: [
    'wow',
    'interesting',
    'why?',
  ],
};

const sendManyWordsAnswer: SolverAnswerType<boolean> = (
  textLower,
  currentSession,
) => {
  const message = getRandomItem(manyWordsMessages[currentSession.locale]);

  return message;
};

export default {
  check: isManyWordsQuestion,
  canRepeat: true,
  answer: sendManyWordsAnswer,
};
