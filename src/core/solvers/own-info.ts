import getRandomItem from 'core/utils/get-random-item';
import { getRandomWordWithAdjective } from 'core/utils/get-random-word';

const answers = {
  en: [
    (name, age) => `just me ${name}`,
    (name, age) => `because I'm ${name}`,
    (name, age) => `and I'm ${name}`,
    (name, age) => `I'm ${name}`,
    (name, age) => `I'm ${name}!`,
    (name, age) => `I'm ${name}!!!`,
    (name, age) => `forgot that I'm ${name}?`,
    (name, age) => `I'm ${name}, and I'm ${age}`,
    (name, age) => `I'm ${name}, and I'm ${age} лет`,
    (name, age) => `forgot that I'm ${name} and I'm ${age}?`,
    (name, age) => `I'm ${age} and I'm ${name}`,
    (name, age) => `I'm ${age}`,
    (name, age) => `I'm ${age}!!!`,
    (name, age) => `I'm only ${age}`,
    (name, age) => `in ${age} it is normal`,
    (name, age) => `in ${age} it isn't normal`,
    (name, age) => `in ${age} is this normal?`,
    (name, age) => `I'm ${name}${age}`,
  ],

  ru: [
    (name, age) => `просто я ${name}`,
    (name, age) => `потому что я ${name}`,
    (name, age) => `это потому что я ${name}`,
    (name, age) => `всё потому что я ${name}`,
    (name, age) => `а всё потому что я ${name}`,
    (name, age) => `это всё потому что я ${name}`,
    (name, age) => `а я ${name}`,
    (name, age) => `я же ${name}`,
    (name, age) => `ну я же ${name}`,
    (name, age) => `а ещё я ${name}`,
    (name, age) => `я ${name}`,
    (name, age) => `я ${name}!`,
    (name, age) => `я ${name}!!!`,
    (name, age) => `забыл, что я ${name}?`,
    (name, age) => `я ${name}, и мне ${age}`,
    (name, age) => `я ${name}, и мне ${age} лет`,
    (name, age) => `забыл, что я ${name}, и мне ${age}?`,
    (name, age) => `забыл, что я ${name}, и мне ${age} лет?`,
    (name, age) => `а я ${name}, и мне ${age}`,
    (name, age) => `а я ${name}, и мне ${age} лет`,
    (name, age) => `мне ${age}, и я ${name}`,
    (name, age) => `мне ${age}`,
    (name, age) => `мне ${age}!!!`,
    (name, age) => `мне же ${age}`,
    (name, age) => `ну мне же ${age}`,
    (name, age) => `мне же всего ${age}`,
    (name, age) => `мне же всего ${age} лет`,
    (name, age) => `а вот мне же всего ${age} лет`,
    (name, age) => `в ${age} это нормально`,
    (name, age) => `в ${age} это ненормально`,
    (name, age) => `в ${age} это нормально?`,
    (name, age) => `для ${age} в самый раз`,
    (name, age) => `я ${name}${age}`,
    (name, age) => `я уронила ${getRandomWordWithAdjective('ru', 'v', 'single', {
      live: [false],
      containable: ['none'],
    })}`,
    (name, age) => `я уронила ${getRandomWordWithAdjective('ru', 'v', 'single', {
      live: [false],
      containable: ['none'],
    })} в ${getRandomWordWithAdjective('ru', 'v', 'single', {
      live: [false],
      containable: ['small'],
    })}`,
    (name, age) => `я положила ${getRandomWordWithAdjective('ru', 'v', 'single', {
      live: [false],
      containable: ['none', 'small'],
    })} в ${getRandomWordWithAdjective('ru', 'v', 'single', {
      live: [false],
      containable: ['small', 'big'],
    })}`,
    (name, age) => `я показала ${getRandomWordWithAdjective('ru', 'd', 'single', {
      live: [true],
    })} ${getRandomWordWithAdjective('ru', 'v', 'single', {
      live: [false],
    })}`,
    (name, age) => `я завела ${getRandomWordWithAdjective('ru', 'v', 'single', {
      live: [true],
    })}`,
  ],
};

export default {
  check: () => Math.random() > 0.95,

  canRepeat: true,

  answer: (textLower, currentSession) => {
    const answer = getRandomItem(answers[currentSession.locale]);

    return answer(getRandomItem(currentSession.name), currentSession.age);
  },
};
