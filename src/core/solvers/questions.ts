import getRandomItem from 'core/utils/get-random-item';
import {
  getRandomAdjective,
  getRandomWord,
  getRandomWordWithAdjective,
  getRandomAdjectiveWithWord,
} from 'core/utils/get-random-word';
import names from 'core/wordbooks/merge/names';
import { getRandomCity } from 'core/utils/getRandomCity';

import type {
  SessionType,
  FullSendMessageType,
  SolverType,
} from 'core/types';
import { getRandomSingleVerb, getRandomVerbWithWord } from 'core/utils/getRandomVerb';

type QuestionType =
  | 'or'
  | 'want'
  | 'love'
  | 'like'
  | 'withWhatF'
  | 'what'
  | 'withWhat'
  | 'whatFor'
  | 'whatF'
  | 'whatA'
  | 'whatKind'
  | 'withWhatKind'
  | 'whatKind2'
  | 'whatKind3'
  | 'how'
  | 'who'
  | 'where'
  | 'whereFrom'
  | 'whereTo'
  | 'inTermsOf'
  | 'why'
  | 'costume'
  | 'shoes'
  | 'unknown';

const classifyQuestion = (textLower: string, currentSession: SessionType): QuestionType => {
  if (currentSession.locale !== 'ru') {
    return null;
  }

  if (textLower.includes(' или ')) {
    return 'or';
  }

  if (
    textLower.includes('хочеш')
  ) {
    return 'want';
  }

  if (
    textLower.includes('любиш')
  ) {
    return 'love';
  }

  if (
    textLower.includes('нрав')
  ) {
    return 'like';
  }

  if (
    textLower.includes('с какой')
  ) {
    return 'withWhatF';
  }

  if (
    textLower.startsWith('какой ') ||
    textLower.includes('какой?') ||
    textLower === 'какой'
  ) {
    return 'what';
  }

  if (
    textLower.includes('с каким')
  ) {
    return 'withWhat';
  }

  if (
    textLower.includes('зачем')
  ) {
    return 'whatFor';
  }

  if (
    textLower.startsWith('какая ') ||
    textLower.includes('какая?') ||
    textLower === 'какая'
  ) {
    return 'whatF';
  }

  if (
    textLower.startsWith('какое ') ||
    textLower.includes('какое?') ||
    textLower === 'какое'
  ) {
    return 'whatA';
  }

  if (
    textLower.startsWith('какие ') ||
    textLower.includes('какие?') ||
    textLower === 'какие'
  ) {
    return 'whatKind';
  }

  if (
    textLower.includes('с какими')
  ) {
    return 'withWhatKind';
  }

  if (
    textLower.includes('в') && (
      textLower.startsWith('каких ') ||
      textLower.includes('каких?') ||
      textLower === 'каких'
    )
  ) {
    return 'whatKind2';
  }

  if (
    textLower.startsWith('каких ') ||
    textLower.includes('каких?') ||
    textLower === 'каких'
  ) {
    return 'whatKind3';
  }

  if (
    textLower.startsWith('как ') ||
    textLower.includes('как?') ||
    textLower === 'как'
  ) {
    return 'how';
  }

  if (
    textLower.startsWith('кто ') ||
    textLower.includes('кто?') ||
    textLower === 'кто'
  ) {
    return 'who';
  }

  if (
    textLower.startsWith('где') ||
    (textLower.includes('где') && textLower.includes('?'))
  ) {
    return 'where';
  }

  if (
    textLower.includes('от куда')
    || textLower.includes('откуда')
  ) {
    return 'whereFrom';
  }

  if (
    textLower.includes('куда')
  ) {
    return 'whereTo';
  }

  if (
    textLower.includes('в') &&
    textLower.includes('смысле')
  ) {
    return 'inTermsOf';
  }

  if (
    textLower.includes('почему')
  ) {
    return 'why';
  }

  if (
    textLower.includes('что')
    && (
      textLower.includes('одет')
      || textLower.includes('надет')
    )
  ) {
    return 'costume';
  }

  if (
    textLower.includes('что')
    && textLower.includes('обут')
  ) {
    return 'shoes';
  }

  if (
    textLower.includes('?')
  ) {
    if (Math.random() < 0.5) {
      return 'unknown';
    }
  }

  return null;
}

type AnswerType = FullSendMessageType | ((arg?: any) => FullSendMessageType);

const answers: Record<QuestionType, AnswerType[]> = {
  or: [
    'ни то, ни другое',
    'ничего из этого',
    (suggestions) => getRandomItem(suggestions),
    (suggestions) => suggestions
      .map((s) => `и ${s}`)
      .join(', '),

    (suggestions) => `конечно ${getRandomItem(suggestions)}`,
    (suggestions) => `только ${getRandomItem(suggestions)}`,

    (suggestions) => ({
      message: getRandomItem(suggestions),
      emotion: 'laught',
    }),

    (suggestions) => ({
      message: `что за ${getRandomItem(suggestions)}?`,
      emotion: 'not_understand',
    }),

    (suggestions) => ({
      message: `причём тут ${getRandomItem(suggestions)}?`,
      emotion: 'not_understand',
    }),

    (suggestions) => ({
      message: `почему ${getRandomItem(suggestions)}?`,
      emotion: 'not_understand',
    }),
  ],

  want: [
    'да',
    'нет',
    'конечно',
    'хочу',
    'не хочу',
    'может быть',
    () => `хочу ${getRandomWordWithAdjective('ru', 'v')}`,
    () => `я хочу ${getRandomWordWithAdjective('ru', 'v')}`,
    () => `я хочу только ${getRandomWordWithAdjective('ru', 'v')}`,
    () => `нет, я хочу только ${getRandomWordWithAdjective('ru', 'v')}`,
    () => `у меня уже есть ${getRandomWordWithAdjective('ru', 'i')}`,
    () => `а ты хочешь ${getRandomWordWithAdjective('ru', 'v')}?`,
  ],

  love: [
    'да',
    'нет',
    'конечно',
    'люблю',
    'не люблю',
    'может быть',
    () => `люблю ${getRandomWordWithAdjective('ru', 'v')}`,
    () => `я люблю ${getRandomWordWithAdjective('ru', 'v')}`,
    () => `я люблю только ${getRandomWordWithAdjective('ru', 'v')}`,
    () => `нет, я люблю только ${getRandomWordWithAdjective('ru', 'v')}`,
    () => `у меня уже есть ${getRandomWordWithAdjective('ru', 'i')}`,
    () => `а ты любишь ${getRandomWordWithAdjective('ru', 'v')}?`,
  ],

  like: [
    'да',
    'нет',
    'конечно',
    'нравится',
    'не нравится',
    'может быть',
    () => `нравится ${getRandomWordWithAdjective('ru', 'i', 'single')}`,
    () => `мне нравится ${getRandomWordWithAdjective('ru', 'i', 'single')}`,
    () => `нравятся ${getRandomWordWithAdjective('ru', 'i', 'multiple')}`,
    () => `мне нравятся ${getRandomWordWithAdjective('ru', 'i', 'multiple')}`,
    () => `нравится только ${getRandomWordWithAdjective('ru', 'i', 'single')}`,
    () => `мне нравится только ${getRandomWordWithAdjective('ru', 'i', 'single')}`,
    () => `нравятся только ${getRandomWordWithAdjective('ru', 'i', 'multiple')}`,
    () => `мне нравятся только ${getRandomWordWithAdjective('ru', 'i', 'multiple')}`,
    () => `нет, мне нравится только ${getRandomWordWithAdjective('ru', 'i', 'single')}`,
    () => `нет, мне нравятся только ${getRandomWordWithAdjective('ru', 'i', 'multiple')}`,
    () => `у меня уже есть ${getRandomWordWithAdjective('ru', 'i')}`,
    () => `а тебе нравится ${getRandomWordWithAdjective('ru', 'i', 'single')}?`,
    () => `а тебе нравятся ${getRandomWordWithAdjective('ru', 'i', 'multiple')}?`,
  ],

  who: [
    'конь в пальто',
    'дед Пихто',
    'кто-то',
    'ты',
    'он',
    'она',
    () => getRandomWordWithAdjective('ru', 'i', 'single', {
      live: [true],
    }),
    () => getRandomItem(getRandomItem(names.ru.manNames)),
    () => getRandomItem(getRandomItem(names.ru.womanNames)),
  ],

  what: [
    () => getRandomAdjective('ru', 'm', 'i', 'single'),
  ],

  withWhat: [
    () => `с ${getRandomAdjective('ru', 'm', 't', 'single')}`,
  ],

  whatF: [
    () => getRandomAdjective('ru', 'f', 'i', 'single'),
  ],

  withWhatF: [
    () => `с ${getRandomAdjective('ru', 'f', 't', 'single')}`,
  ],

  whatA: [
    () => getRandomAdjective('ru', 'a', 'i', 'single'),
  ],

  whatKind: [
    () => getRandomAdjective('ru', 'multiple'),
  ],

  withWhatKind: [
    () => `с ${getRandomAdjective('ru', 'm', 't', 'multiple')}`,
  ],

  whatKind2: [
    () => `в ${getRandomAdjective('ru', 'multiple', 'p')}`,
  ],

  whatKind3: [
    () => getRandomAdjective('ru', 'multiple', 'p'),
  ],

  whatFor: [
    'затем',
    'так норм',
    'так надо',
    'мне так нравится',
    'мне так больше нравится',
    'а разве это плохо',
    'как будто, это что-то плохое',
    'это прекрасно',
    'это здорово',
    'это замечательно',
    'как умею',
    'а как надо?',
    'а почему нет?',
    'а ты как делаешь?',
  ],

  why: [
    'потому',
    'потому что',
    () => `потому что ${getRandomAdjectiveWithWord('ru')}`,
    () => `потому что ты ${getRandomAdjective('ru', 'm', 'i', 'single')}`,
    () => `потому что ты не ${getRandomAdjective('ru', 'm', 'i', 'single')}`,
    () => `потому что ты ${getRandomSingleVerb('ru', 'continous', 'present', 'indicative', 'you', 'm', 'single')}`,
    () => `потому что ты ${getRandomVerbWithWord('ru', 'perfect', 'past', 'indicative', 'you', 'm', 'single')}`,
  ],

  how: [
    'так',
    'вот так',
    'вот так вот',
    {
      message: 'плохо',
      emotion: 'sad',
    },
    'хорошо',
    'много',
    {
      message: 'мало',
      emotion: 'sad',
    },
    'сильно',
    {
      message: 'слабо',
      emotion: 'sad',
    },
    'нормально',
    'отлично',
    'замечательно',
  ],

  where: [
    'в Караганде',
    'в бороде',
    'у тебя в бороде',
    'на кухне',
    'на улице',
    'под столом',
    'на столе',
    'на стене',
    'под потолком',
    'в углу',
    'на крыше',
    'за границей',
    'под землёй',
    'в саду',
    'в огороде',
    'на Луне',
    'на Марсе',
    'в метро',
    'на работе',
    'в отпуске',
    'в океане',
    'в море',
    'на корабле',
    'там',
    'где-то',
    () => `у ${getRandomWordWithAdjective('ru', 'r', 'any', {
      live: [true],
    })}`,
    () => `в ${getRandomWordWithAdjective('ru', 'p', 'single', {
      containable: ['small', 'big'],
    })}`,
    () => `в ${getRandomCity('ru', 'p')}`,
  ],

  whereFrom: [
    'оттуда',
    'оттуда, откуда надо',
    'от верблюда',
    'от бабушки',
    'с ютуба',
    'с авито',
    'с другой планеты',
    'на лестнице нашли',
    'подарили',
    'отдали',
    'продали',
    'купили',
    'позвали',
    'откуда-то',
    'с дерева',
    'из окна',
    'из речки',
    () => `из ${getRandomWordWithAdjective('ru', 'r', 'single', {
      containable: ['small', 'big'],
    })}`,
    () => `в ${getRandomWordWithAdjective('ru', 'p', 'single', {
      containable: ['small', 'big'],
    })} нашли`,
    () => `из ${getRandomCity('ru', 'r')}`,
  ],

  whereTo: [
    'туда',
    'налево',
    'направо',
    'прямо',
    'в небо',
    'на кухню',
    'на мост',
    'в центр',
    'на море',
    'в гору',
    'домой',
    'на работу',
    'за пивом',
    () => `в ${getRandomWordWithAdjective('ru', 'v', 'single', {
      containable: ['small', 'big'],
    })}`,
    () => `в ${getRandomCity('ru', 'v')}`,
  ],

  inTermsOf: [
    'в прямом',
    'в переносном',
    'в коромысле',
    'в глубоком',
    'в широком',
    'без смысла',
    'поразмысли',
    'переосмысли',
  ],

  costume: [
    () => getRandomWord('ru', 'i', 'single', {
      costume: [true],
    }),
  ],

  shoes: [
    () => getRandomWord('ru', 'i', 'multiple', {
      shoes: [true],
    }),
  ],

  unknown: [
    'да',
    'ага',
    'нет',
    'неа',
    'наверно',
    'не знаю',
    {
      message: 'не знаю',
      emotion: 'laught',
    },
    {
      message: 'не знаю',
      emotion: 'not_understand',
    },
    'тоже не знаю',
    'надо подумать',
    'не знаешь',
    'не знаешь чтоли?',
    {
      message: 'загадка',
      emotion: 'laught',
    },
    'отгадай',
    {
      message: 'отгадай',
      emotion: 'laught',
    },
    'угадай',
    'догадайся',
    'а ты подумай',
    'а ты подумай получше',
    'подумай получше',
    'не догадываешься',
    'а что?',
    'да не',
    'вроде',
    'хехе',
  ],
};

export const questions: SolverType<QuestionType> = {
  check: (textLower, currentSession) => classifyQuestion(textLower, currentSession),

  canRepeat: true,

  answer: (textLower, currentSession, classifier) => {
    const answer = getRandomItem(answers[classifier]);

    if (classifier === 'or') {
      const suggestions = textLower
        .split(/хочешь|хочеш|хочиш|,|или|\?/)
        .map((s) => s.trim())
        .filter(Boolean);

        if (typeof answer === 'function') {
          return answer(suggestions);
        }
    
        return answer;
    }

    if (typeof answer === 'function') {
      return answer();
    }

    return answer;
  },
}
