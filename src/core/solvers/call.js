import getRandomItem from 'core/utils/get-random-item';

const answers = {
  en: [
    'what?',
    'what??',
    'how can i help?',
    'can i help you?',
    'yes it\'s my name',
    'it\'s my name',
    'it\'s me',
    'it\'s me!',
    'yes it\'s me',
    'yes it\'s me!',
    'do you like my name',
  ],

  ru: [
    'что?',
    'что??',
    'чего?',
    'чего??',
    'чо?',
    'чо??',
    'цо?',
    'ась?',
    'чем помочь?',
    'чем могу помочь?',
    'чем могу быть полезна?',
    'да, меня так зовут',
    'меня так зовут',
    'это я',
    'это я!',
    'да это же я',
    'да, это я',
    'да, это я!',
    'я!',
    'нравится моё имя?',
    'нравится, как меня зовут?',
  ],
};

export default {
  check: (textLower, currentSession) => {
    if (!currentSession.name) {
      return false;
    }

    return currentSession.nameLower
      .some((name) => textLower.includes(name));
  },

  canRepeat: true,

  answer: (textLower, currentSession) => {
    if (currentSession.opponent.name) {
      if (Math.random() < 0.2) {
        const nameFromCall = currentSession.nameLower
          .find((name) => textLower.includes(name));

        return textLower.replace(
          nameFromCall,
          getRandomItem(currentSession.opponent.name),
        );
      }
    }

    return getRandomItem(answers[currentSession.locale]);
  },
};
