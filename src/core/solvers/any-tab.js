import {
  isAllowed,
  getRandomMessage,
} from 'core/tabs-sync';

export default {
  check: (textLower, currentSession) => {
    if (!currentSession.locale === 'ru') {
      return false;
    }

    if (!isAllowed()) {
      return false;
    }

    return Math.random() < 0.3;
  },

  canRepeat: true,

  answer: getRandomMessage,
};
