import getRandomItem from 'core/utils/get-random-item';
import { getRandomWordWithAdjective } from 'core/utils/get-random-word';
import names from 'core/wordbooks/merge/names';

import type {
  LocaleType,
  SolverCheckType,
  SolverAnswerType,
} from 'core/types';

const isRelationshipQuestion: SolverCheckType<boolean> = (
  textLower,
  currentSession,
) => {
  switch (currentSession.locale) { 
    case 'ru':
      return (
        (
          textLower.includes('есть') && (
            textLower.includes('парень') ||
            textLower.includes('муж') ||
            textLower.includes('супруг')
          )
        )
        || textLower.includes('замужем')
        || textLower.includes('одинока')
        || (
          textLower.includes('ты') && (
            textLower.includes('одна')
          )
        )
      );
        
    case 'en':
      return (
        (
          textLower.includes('have') && (
            textLower.includes('bf') ||
            textLower.includes('boy') ||
            textLower.includes('friend')
          )
        )
        || (
          textLower.includes('you') && (
            textLower.includes('merried') ||
            textLower.includes('alone')
          )
        )
      );

    default:
      return false;
  }
};

type RelationshipMessageType = string | (() => string);

const connectingStringsRu = [
  'если есть',
  'если уже есть',
  'если у меня есть',
  'когда есть',
  'когда уже есть',
  'когда у меня есть',
];

const getConnectingStringRu = (): string => getRandomItem(connectingStringsRu);

const relationshipMessages: Record<LocaleType, RelationshipMessageType[]> = {
  ru: [
    'неа',
    'нет парня у меня',
    'нет мужика',
    () => `нет парня, но есть ${getRandomWordWithAdjective('ru')}`,
    () => `зачем мне парень, ${getConnectingStringRu()} ${getRandomWordWithAdjective('ru')}`,
    () => `у меня есть только ${getRandomWordWithAdjective('ru')}`,
    () => `${getRandomWordWithAdjective('ru')} лучше, чем парень`,
    () => `зачем мне парень?`,
    () => `зачем мне ${getRandomItem(getRandomItem(names.ru.manNames))}?`,
    () => `зачем мне какой-то ${getRandomItem(getRandomItem(names.ru.manNames))}?`,
    () => `зачем мне ${getRandomItem(getRandomItem(names.ru.manNames))} или ${getRandomItem(getRandomItem(names.ru.manNames))}?`,
    () => `зачем мне какой-то ${getRandomItem(getRandomItem(names.ru.manNames))} или ${getRandomItem(getRandomItem(names.ru.manNames))}?`,
    () => `зачем мне ${getRandomItem(getRandomItem(names.ru.manNames))}, ${getConnectingStringRu()} ${getRandomWordWithAdjective('ru')}?`,
    () => `зачем мне ${getRandomItem(getRandomItem(names.ru.manNames))} или ${getRandomItem(getRandomItem(names.ru.manNames))}, ${getConnectingStringRu()} ${getRandomWordWithAdjective('ru')}?`,
  ],
  
  en: [
    'no',
    'I\'m alone',
  ],
};

const sendRelationshipAnswer: SolverAnswerType<boolean> = (
  textLower,
  currentSession,
) => {
  const message = getRandomItem(relationshipMessages[currentSession.locale]);

  if (typeof message === 'function') {
    return message();
  }

  return message;
};

export default {
  check: isRelationshipQuestion,
  canRepeat: true,
  answer: sendRelationshipAnswer,
};
