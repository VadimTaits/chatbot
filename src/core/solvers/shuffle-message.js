import shuffle from 'lodash.shuffle';

import processEveryWord from '../utils/process-every-word';

const re = /[а-яё]+/gim;

export default {
  check: (textLower, currentSession) => {
    if (Math.random() > 0.1) {
      return null;
    }

    const matches = textLower.match(re);

    if (matches && matches.length > 2) {
      return {
        words: matches,
        isFirst: true,
        text: textLower,
      };
    }

    return null;
  },

  tryRepeat: ({
    words,
    isFirst,
  }) => {
    if (Math.random() > 0.1) {
      return null;
    }

    return {
      words,
      isFirst: false,
    };
  },

  canRepeat: true,

  answer: (textLower, currentSession, {
    isFirst,
    words,
    text,
  }) => {
    const matches = textLower.match(re);

    const indexes = words.map((word, index) => index);
    const shuffledIndexes = shuffle(indexes);

    return processEveryWord(text, (subword, index) => {
      return words[shuffledIndexes[index]];
    });
  },
};
