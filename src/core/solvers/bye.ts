import getRandomItem from 'core/utils/get-random-item';

import type {
  LocaleType,
  SolverCheckType,
  SolverAnswerType,
} from 'core/types';

const isByeQuestion: SolverCheckType<boolean> = (
  textLower,
  currentSession,
) => {
  switch (currentSession.locale) { 
    case 'ru':
      return (
        textLower.includes('пока')
        || textLower.includes('спокойной')
        || textLower.includes('споки')
        || (
          textLower.includes('я ')
          && textLower.includes('спать')
        )
      );
        
    case 'en':
      return (
        textLower.includes('bye')
      );

    default:
      return false;
  }
};

const byeMessages: Record<LocaleType, string[]> = {
  ru: [
    'неееет',
    'ну неет',
    'не уходи',
    'не покидай меня',
    'с тобой интересно',
    'давай ещё поговорим',
    'мне скучно',
    'расскажи ещё что-нибудь',
  ],
  
  en: [
    'no',
  ],
};

const sendByeAnswer: SolverAnswerType<boolean> = (
  textLower,
  currentSession,
) => {
  const message = getRandomItem(byeMessages[currentSession.locale]);

  return message;
};

export default {
  check: isByeQuestion,
  canRepeat: true,
  answer: sendByeAnswer,
};
