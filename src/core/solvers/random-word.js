import { getRandomWordWithAdjective } from 'core/utils/get-random-word';

export default {
  check: () => true,
  canRepeat: true,
  answer: (textLower, currentSession) => getRandomWordWithAdjective(currentSession.locale),
};
