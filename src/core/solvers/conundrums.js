import conundrums from 'core/wordbooks/merge/conundrums';
import getRandomItem from 'core/utils/get-random-item';

const prefixes = {
  no: [
    '',
    'нет, ',
    'это ',
    'не правильно, ',
  ],

  yes: [
    'да',
    'угадал',
    'молодец',
    'ого',
  ],
};

export default {
  check: (textLower, currentSession) => {
    if (currentSession.locale !== 'ru' || Math.random() > 0.1) {
      return null;
    }

    const allConundrums = conundrums[currentSession.locale];

    const conundrumIndex = Math.floor(Math.random() * allConundrums.length);

    if (currentSession.usedConundrums[conundrumIndex]) {
      return null;
    }

    currentSession.usedConundrums[conundrumIndex] = true;

    return {
      question: allConundrums[conundrumIndex],
      state: 'question',
    };
  },

  tryRepeat: ({
    question,
    state,
  }) => {
    if (state !== 'question') {
      return null;
    }

    return {
      question,
      state: 'ask',
    };
  },

  canRepeat: true,

  answer: (textLower, currentSession, {
    question,
    state,
  }) => {
    switch (state) {
      case 'question':
        return question[0];

      default:
        if (textLower.includes(question[1])) {
          return getRandomItem(prefixes.yes);
        } else {
          return `${getRandomItem(prefixes.no)}${question[1]}`;
        }
    }
  },
};
