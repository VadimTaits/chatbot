import getRandomItem from '../utils/get-random-item';
import {
  getRandomSingleVerb,
  getRandomVerbWithWord,
} from '../utils/getRandomVerb';

import type {
  LocaleType,
  FullSendMessageType,
  SolverCheckType,
  SolverAnswerType,
  SolverType,
} from '../types';

const isActivityDetectQuestion: SolverCheckType<boolean> = (textLower, currentSession) => {
  switch (currentSession.locale) { 
    case 'ru':
      return (
        (
          textLower.includes('что')
        ) && (
          textLower.includes('делаешь')
        )
      ) || (
        (
          textLower.includes('чем')
        ) && (
          textLower.includes('маешься')
          || textLower.includes('занимаетесь')
          || textLower.includes('занимаешься')
          || textLower.includes('занят')
          || textLower.includes('занята')
        )
      ) || (
        textLower.includes('не спишь')
      );

    default:
      return false;
  }
};

const activities = [
  'общаюсь с тобой',
  'плачу налоги',
  'плаваю',
  'переодеваюсь',
  'смотрю',
  'кручу спиннер',
  'прыгаю',
  'бегаю',
  'выбираю',
  'лечу',
  'убираю',
  'скачу',
  'вышиваю',
  () => getRandomSingleVerb('ru', 'continous', 'present', 'indicative', 'i', 'f', 'single'),
  () => getRandomVerbWithWord('ru', 'continous', 'present', 'indicative', 'i', 'f', 'single'),
];

const questions: Record<LocaleType, FullSendMessageType[]> = {
  en: [
    'what are you doing?',
  ],

  ru: [
    'что делаешь?',
    'что ты делаешь?',
    'а что ты делаешь?',
    'ты что делаешь?',
    'ты что сейчас делаешь?',
    'чем ты занят?',
    'чем занят?',
    'чем занимаешься?',
    'чем ты занимаешься?',
    'а чем ты занимаешься?',
  ],
};

const sendActivityAnswer: SolverAnswerType<boolean> = (textLower, currentSession) => {
  if (!currentSession.activity || currentSession.activityExpired > Date.now()) {
    const activity = getRandomItem(activities);

    currentSession.activity = typeof activity === 'function'
      ? activity()
      : activity;

    currentSession.activityExpired = Date.now() + 300000;
  }

  return currentSession.activity.toString();
}

export const activity: SolverType<boolean> = {
  check: isActivityDetectQuestion,
  canRepeat: true,
  answer: sendActivityAnswer,

  sendOwnMessage: (send, currentSession) => {
    if (!currentSession.solversPayload.hello?.sent) {
      return;
    }

    if (Math.random() > 0.2) {
      return;
    }

    send(getRandomItem(questions[currentSession.locale]));
  },
};
