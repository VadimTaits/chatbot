import getRandomItem from 'core/utils/get-random-item';

import {
  manNames,
  manNamesLowerFlatten,
  manNamesToGroupsMap,
} from 'core/wordbooks/ru/names';

function isNameDetect(textLower, currentSession) {
  if (!currentSession.opponent.name) {
    const nameIndex = manNamesLowerFlatten
      .findIndex((nameLower) => textLower.includes(nameLower));

    if (nameIndex !== -1) {
      const nameLower = manNamesLowerFlatten[nameIndex];

      const groupIndex = manNamesToGroupsMap[nameLower];
      const nameGroup = manNames[groupIndex];

      currentSession.opponent.name = nameGroup;

      return true;
    }
  }
  
  switch (currentSession.locale) { 
    case 'ru':
      return (
        textLower.includes('имя') ||
        textLower.includes('зовут') ||
        textLower.includes('звать') ||
        textLower.includes('завут') ||
        textLower.includes('представься')
      );
        
    case 'en':
      return (
        textLower.includes('name')
      );

    default:
      return false;
  }
}

export default {
  check: isNameDetect,

  tryRepeat: () => false,
  canRepeat: false,

  answer: (textLower, currentSession) => {
    return getRandomItem(currentSession.name);
  },
};
