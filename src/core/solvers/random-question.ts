import questions from 'core/wordbooks/merge/questions';

import type {
  SessionType,
  SolverType,
} from '../types';

const pickQuestion = (session: SessionType): string => {
  const allQuesions = questions[session.locale];

  const questionIndex = Math.floor(Math.random() * allQuesions.length);

  if (session.usedQuestions[questionIndex]) {
    return null;
  }

  session.usedQuestions[questionIndex] = true;

  return allQuesions[questionIndex];
};

export const randomQuestion: SolverType<string> = {
  check: (textLower, currentSession) => {
    if (currentSession.locale !== 'ru' || Math.random() > 0.1) {
      return null;
    }

    return pickQuestion(currentSession);
  },

  canRepeat: false,

  answer: (textLower, currentSession, answer) => answer,

  sendOwnMessage: (send, currentSession) => {
    if (currentSession.locale !== 'ru' || Math.random() > 0.1) {
      return null;
    }

    send(pickQuestion(currentSession));
  },
};
