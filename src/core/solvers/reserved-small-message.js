import getRandomItem from 'core/utils/get-random-item';
import { getRandomWordWithAdjective } from 'core/utils/get-random-word';

const re = /[^a-яё]/gim;

function classifySmallMessage(textLower, currentSession) {
  if (currentSession.locale !== 'ru') {
    return null;
  }

  const onlyLetters = textLower.split(re).join('');

  switch (onlyLetters) {
    case 'да':
      return 'yes';

    case 'нет':
      return 'no';

    case 'не':
      return 'no2';

    case 'ну':
      return 'well';

    case 'я':
      return 'i';

    case 'ты':
      return 'you';

    case 'а':
      return 'a';

    case 'и':
      return 'and';

    default:
      return null;
  }
}

const universalAnswers = [
  'да',
  'понятно',
  'ясно',
  'хорошо',
  'отлично',
  'точно?',
  'точно да?',
  'точно нет?',
  'почему да?',
  'почему нет?',
  'почему?',
  'а почему?',
  'зачем?',
  'а зачем?',
  'нет',
  'нет?',
  'почему нет?',
  'не да',
  'да?',
  'да??',
  'дадада',
  'да да',
  'и что, что да?',
  'я и говорю, что да',
  'я и говорю, что нет',
  'ну так',
  '?',
];

const answers = {
  yes: universalAnswers.concat([
    'звезда',
    'борода',
    'балда',
    'ерунда',
    'тамада',
    'вода',
    'узда',
    'та',
    'сковорода',
    'слобода',
    'нужна',
    'еда',
  ]),

  no: universalAnswers.concat([
    'чей ответ?',
    'винегрет',
    'злобный людоед;',
    'старый Магомед',
    'из яйца омлет',
    'балет',
    'поэт',
    'обед',
    'совет',
    'куплет',
    'портрет',
    'секрет',
    'голубой карсет',
    'из жука паштет',
    'ягодный рулет',
  ]),

  'no2': universalAnswers.concat([
    'ме',
    'бе',
    'де',
    'злобный людоед;',
    'старый Магомед',
    'из яйца омлет',
    'балет',
    'поэт',
    'обед',
    'совет',
    'куплет',
    'портрет',
    'секрет',
    'голубой карсет',
    'из жука паштет',
    'ягодный рулет',
  ]),

  well: [
    'му',
    'муму',
    'бу',
    'бу!',
    'ну-ну',
    'гну',
    'пырну',
    'прыгну',
    'баранки гну',
    'да ну',
    'и ну',
    'и что',
  ],

  i: [
    'ты',
    'ты?',
    'нет, я',
    'да, ты',
    'именно ты',
    'конечно, ты',
    'я',
    'почему я?',
    'почему ты?',
    'нет, блин, я',
    'последняя буква в алфавите',
    'понятно, что ты',
    'а кто ещё?',
    () => `да, ты ${getRandomWordWithAdjective('ru', 'i', 'single')}`,
    () => `потому что у тебя есть ${getRandomWordWithAdjective('ru')}`,
    () => `ты, потому что у тебя есть ${getRandomWordWithAdjective('ru')}`,
    () => `ведь ты ${getRandomWordWithAdjective('ru', 'i', 'single')}`,
    () => `потому что ты ${getRandomWordWithAdjective('ru', 'i', 'single')}`,
    () => `${getRandomWordWithAdjective('ru', 'i', 'single')} ты`,
  ],

  you: [
    'я',
    'ты',
    'да, ты',
    'нет, ты',
    'ты?',
    'я?',
    'нет, ты',
    'да, я',
    'да, ты',
    'именно я',
    'именно ты',
    'конечно, я',
    'конечно, ты',
    'почему я?',
    'почему ты?',
    'не ты',
    'не я',
    'блако',
    'я и говорю, что ты',
    'понятно, что ты',
    'а кто ещё?',
    () => `а ты ${getRandomWordWithAdjective('ru', 'i', 'single')}`,
    () => `а ты вообще ${getRandomWordWithAdjective('ru', 'i', 'single')}`,
    () => `а я не ${getRandomWordWithAdjective('ru', 'i', 'single')}`,
    () => `я не ${getRandomWordWithAdjective('ru', 'i', 'single')}`,
  ],

  a: [
    'б',
    'а',
    'к47',
    'не акай',
    'и б сидели на трубе',
    'лфавит',
    'алфавит учишь?',
  ],

  and: [
    'ы',
    'и я',
    'и ты',
    () => `и ${getRandomWordWithAdjective('ru', 'i', 'single')}`,
    () => `и ${getRandomWordWithAdjective('ru', 'i', 'single')} тоже`,
    () => `и ${getRandomWordWithAdjective('ru', 'i', 'single')}, конечно же`,
  ],
};

export default {
  check: (textLower, currentSession) => {
    return classifySmallMessage(textLower, currentSession);
  },

  canRepeat: true,

  answer: (textLower, currentSession, classifier) => {
    const answer = getRandomItem(answers[classifier]);

    if (typeof answer === 'function') {
      return answer();
    }

    return answer;
  },
}
