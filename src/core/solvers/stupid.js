import getRandomItem from 'core/utils/get-random-item';

function classifyStupid(textLower, currentSession) {
  switch (currentSession.locale) {
    case 'ru':
      return textLower.includes('пошлая') ||
        textLower.includes('пошлый') ||
        textLower.includes('пошленькая') ||
        textLower.includes('хочешь пошлого') ||
        textLower.includes('интим') ||
        textLower.includes('пошалить') ||
        textLower.includes('пошалим') ||
        textLower.includes('го вирт') ||
        textLower === 'вирт' ||
        textLower.startsWith('вирт ') ||
        textLower.includes('вирт?') ||
        textLower.includes('мастурбиру') ||
        (
          (
            textLower.includes('ласкать') ||
            textLower.includes('ласкаешь')
          ) &&
          textLower.includes('себя')
        ) ||
        (
          textLower.includes('ищу') &&
          (
            textLower.includes('госпожу') ||
            textLower.includes('девушку') ||
            textLower.includes('пошленькую') ||
            textLower.includes('пошлую')
          )
        ) ||
        (
          textLower.includes('кончит') &&
          (
            textLower.includes('хочу') ||
            textLower.includes('поможеш')
          )
        ) ||
        (
          (
            textLower.includes('член') ||
            textLower.includes('кончу')
          ) &&
          (
            textLower.includes('рот') ||
            textLower.includes('оцениш') ||
            textLower.includes('хочеш') ||
            textLower.includes('кину') ||
            textLower.includes('видет') ||
            textLower.includes('покаж') ||
            textLower.includes('показать')
          )
        ) ||
        textLower.includes('похотлив') ||
        (
          (
            textLower.includes('кинь') ||
            textLower.includes('пришли')
          ) && (
            textLower.includes('сиськи') ||
            textLower.includes('титьки') ||
            textLower.includes('тити')
          )
        ) ||
        textLower === 'сиськи';

    default:
      return false;
  }
}

const stupidMessages = [
  'ты тупой',
  'ты дебил',
  'ты идиот',
  'ты даун',
  'ты кретин',
];

function sendStupidAnswer() {
  return getRandomItem(stupidMessages);
}

export default {
  check: (textLower, currentSession) => {
    const classifier = classifyStupid(textLower, currentSession);

    if (classifier) {
      if (
        currentSession.isAngryAllowed ||
        currentSession.isAngryAllowedForStupid
      ) {
        currentSession.isAngry = true;
      }
    }

    return classifier;
  },

  canRepeat: true,

  tryRepeat: (prevRes, textLower) => (
    textLower.includes('?') ||
    textLower.includes('почему') ||
    textLower.includes('ты') ||
    textLower.includes('неа') ||
    textLower.includes('нет') ||
    textLower.includes('что')
  ),

  answer: sendStupidAnswer,
};
