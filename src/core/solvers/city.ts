import getRandomItem from 'core/utils/get-random-item';
import {
  getWordCase,
} from 'core/utils/get-random-word';

import type {
  SolverCheckType,
  SolverAnswerType,
  SessionType,
} from 'core/types';

const isCityDetectQuestion: SolverCheckType<boolean> = (textLower, currentSession) => {
  switch (currentSession.locale) { 
    case 'ru':
      return (
        (
          textLower.includes('ты')
        ) && (
          textLower.includes('откуда')
        )
      ) || (
        textLower.includes('город') && (
          textLower.includes('какой') ||
          textLower.includes('какого') ||
          textLower.includes('?')
        )
      );

    default:
      return false;
  }
}

type AnswerType = (session: SessionType) => string;

const answers: AnswerType[] = [
  ({ city, locale }) => getWordCase(city.name, locale, 'i'),
  ({ city, locale }) => `из ${getWordCase(city.name, locale, 'r')}`,
  ({ city, locale }) => `я из ${getWordCase(city.name, locale, 'r')}`,
];

const sendCityAnswer: SolverAnswerType<boolean> = (textLower, currentSession) => {
  const answer = getRandomItem(answers);

  return answer(currentSession);
}

export default {
  check: isCityDetectQuestion,
  canRepeat: true,
  answer: sendCityAnswer,
};
