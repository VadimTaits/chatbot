import getRandomItem from 'core/utils/get-random-item';

function check(textLower, currentSession) {
  switch (currentSession.locale) {
    case 'ru':
      return textLower.includes('падла') ||
        textLower.includes('ублюдок') ||
        textLower.includes('пидор') ||
        textLower.includes('пидар') ||
        textLower.includes('долбоеб') ||
        textLower.includes('долбаеб') ||
        textLower.includes('долбоёб') ||
        textLower.includes('долбаёб') ||
        textLower.includes('еблан') ||
        textLower.includes('ошибка') ||
        textLower.includes('чмо');

    case 'en':
      return false;

    default:
      return false;
  }
}

const messages = {
  ru: [
    'да, это ты',
    'это ты',
    'нет, ты',
    'да, ты такой',
    'я и говорю, что это ты',
    'ты',
    'да это же ты',
  ],

  en: [
    'hi',
    'hello',
  ],
};

function answer(textLower, currentSession) {
  return getRandomItem(messages[currentSession.locale]);
}

export default {
  check,
  canRepeat: false,
  answer,
};
