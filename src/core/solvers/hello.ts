import getRandomItem from '../utils/get-random-item';

import type {
  LocaleType,
  FullSendMessageType,
  SolverCheckType,
  SolverAnswerType,
  SolverType,
} from '../types';

const isHello: SolverCheckType<boolean> = (textLower, currentSession) => {
  if (currentSession.solversPayload.hello?.sent) {
    return false;
  }

  switch (currentSession.locale) { 
    case 'ru':
      return /хе+й/g.test(textLower) ||
        textLower === 'прив' ||
        textLower.includes('привт') ||
        textLower.includes('првт') ||
        textLower.includes('привет') ||
        textLower.indexOf('приве') === 0 ||
        textLower.indexOf('здаров') === 0 ||
        textLower.indexOf('здравствуй') === 0 ||
        textLower.indexOf('хэй') === 0 ||
        textLower.indexOf('хай') === 0 ||
        textLower.includes('дратути') ||
        textLower.indexOf('доброй ночи') === 0;
        
    case 'en':
      return (
        textLower.includes('hello') ||
        textLower.includes('hey') ||
        textLower.includes('hi')
      );

    default:
      return false;
  }
};

const helloMessages: Record<LocaleType, FullSendMessageType[]> = {
  ru: [
    'привет',
    'привет!',
    'и тебе привет',
    'привеееееет',
    'прив',
    'прив!',
    'хэй',
    'здравствуй',
    'салют!',
    'дратути',
    'даров',
  ],
  
  en: [
    'hi',
    'hello',
  ],
};

const sendHelloAnswer: SolverAnswerType<boolean> = (textLower, currentSession) => {
  if (!currentSession.solversPayload.hello) {
    currentSession.solversPayload.hello = {}
  }

  currentSession.solversPayload.hello.sent = true;

  return getRandomItem(helloMessages[currentSession.locale]);
};

export const hello: SolverType<boolean> = {
  check: isHello,
  canRepeat: false,
  answer: sendHelloAnswer,

  sendOwnMessage: (send, currentSession) => {
    if (currentSession.solversPayload.hello?.sent) {
      return;
    }

    if (!currentSession.solversPayload.hello) {
      currentSession.solversPayload.hello = {}
    }
  
    currentSession.solversPayload.hello.sent = true;

    send(getRandomItem(helloMessages[currentSession.locale]));
  },
};
