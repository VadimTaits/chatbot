import getRandomItem from 'core/utils/get-random-item';

const botAnswers = [
  'а ты кот',
  'сам ты бот',
  'сам ты ботан',
  'что это такое?',
  'я не ботан',
  'какой бот?',
  'какой ещё бот?',
  'почему?',
  'почему это?',
  'эээээ...',
  'что?',
  ':(',
  'ну и что?',
  'тебе со мной не интересно?',
  'это такая программа?',
  'ух ты',
  'да ладно?',
  'как узнал?',
  'а ты?',
  'ну тогда ты лоханка с супом',
  'ахахахах',
  'хаха',
  'очень смешно',
  'повёлся',
  'я бот?',
  'я самый лучший бот',
];

export default {
  check: (textLower, currentSession) => {
    switch (currentSession.locale) { 
      case 'ru':
        return (
          textLower === 'бот' ||
          textLower.includes('ты бот') ||
          textLower.includes('бот?') ||
          (
            textLower.includes('ты') &&
            textLower.includes('программа')
          )
        );

      default:
        return false;
    }
  },

  canRepeat: true,

  answer: () => {
    return getRandomItem(botAnswers);
  },
};
