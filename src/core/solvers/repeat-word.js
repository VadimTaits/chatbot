import { getRandomWordWithAdjective } from 'core/utils/get-random-word';
import processEveryWord from '../utils/process-every-word';

const re = /[а-яё]+/gim;

export default {
  check: (textLower, currentSession) => {
    if (Math.random() > 0.1) {
      return false;
    }

    const matches = textLower.match(re);

    let word;
    if (matches && matches.length > 0) {
      word = matches[matches.length - 1];
    } else {
      word = getRandomWordWithAdjective(currentSession.locale);
    }

    return {
      isFirst: true,
      word,
    };
  },

  tryRepeat: ({
    word,
  }) => {
    if (Math.random() > 0.6) {
      return null;
    }

    return {
      word,
      isFirst: false,
    };
  },

  canRepeat: true,

  answer: (textLower, currentSession, {
    isFirst,
    word,
  }) => {
    if (isFirst) {
      return word;
    }

    const matches = textLower.match(re);

    if (matches && matches.length > 0) {
      const lastMatchIndex = matches.length - 1;

      return processEveryWord(textLower, (subword, index) => {
        if (index === lastMatchIndex) {
          return word;
        }

        return subword;
      });
    }

    return `${word} ${textLower}`;
  },
};
