import getRandomItem from 'core/utils/get-random-item';

function isNicely(textLower, currentSession) {
  switch (currentSession.locale) { 
    case 'ru':
      return textLower.includes('приятно');

    default:
      return false;
  }
}

const nicelyMessages = [
  'и мне',
  'взаимно',
  'очень',
  'мне тоже приятно',
  'ммм',
  'мимими',
  'спасибо, мне тоже',
  'мне тоже',
];

function sendNicelyAnswer() {
  return getRandomItem(nicelyMessages);
}

export default {
  check: isNicely,
  canRepeat: true,
  answer: sendNicelyAnswer,
};
