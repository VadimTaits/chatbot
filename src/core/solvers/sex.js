import getRandomItem from 'core/utils/get-random-item';

function getClassifier(textLower, currentSession) {
  switch (currentSession.locale) {
    case 'en':
      if (
        textLower === 'f' ||
        textLower === 'f?' ||
        textLower.indexOf('fm') === 0 ||
        textLower.indexOf('mf') === 0 ||
        textLower.indexOf('m?') === 0 ||
        textLower.indexOf('m f') === 0 ||
        textLower.indexOf('i\'m m') === 0 ||
        textLower.includes('m or f') ||
        textLower.includes('f or m') ||
        textLower.includes(' f?') ||
        textLower.includes('m/f') ||
        textLower.includes('f/m') ||
        textLower.includes('asl')
      ) {
        return 'm';
      }

      if (
        textLower.includes('male') ||
        textLower.includes('woman') ||
        textLower.includes('female')
      ) {
        return 'full';
      }

      if (
        textLower.includes('sex')
      ) {
        return 'sex';
      }
      break;

    case 'ru':
      if (
        textLower === 'м' ||
        textLower === 'ж?' ||
        textLower.indexOf('мж') === 0 ||
        textLower.indexOf('жм') === 0 ||
        textLower.indexOf('м?') === 0 ||
        textLower.indexOf('м ж') === 0 ||
        textLower.indexOf('м,ж') === 0 ||
        textLower.indexOf('я м') === 0 ||
        textLower.includes('м или ж') ||
        textLower.includes('ты ж?') ||
        textLower.includes('м/ж')
      ) {
        return 'm';
      }

      if (
        textLower === 'п' ||
        textLower === 'д?' ||
        textLower.includes('д или п') ||
        textLower.includes('д п') ||
        textLower.includes('п д')
      ) {
        return 'p';
      }

      if (
        textLower.includes('парень') ||
        textLower.includes('девушка')
      ) {
        return 'full';
      }

      if (
        (textLower.includes('какого') && textLower.includes('пола')) ||
        (textLower.includes('какой') && textLower.includes('пол'))
      ) {
        return 'sex';
      }
      break;

    default:
      return null;
  }
}

function isSexDetectQuestion(textLower, currentSession, messages) {
  const classifier = getClassifier(textLower, currentSession);

  if (!classifier) {
    return null;
  }

  if (messages.length < 10) {
    if (currentSession.isAngryAllowed) {
      currentSession.isAngry = true;
    }
  }

  let withI;
  switch (currentSession.locale) {
    case 'en':
      withI = textLower.includes('i');
      break;

    case 'ru':
      withI = textLower.includes('я');
      break;

    default:
      break;
  }

  return {
    withI,
    classifier,
  };
}

const sexAnswers = {
  m: {
    ru: ['ж'],
    en: ['f'],
  },
  p: {
    ru: ['д'],
    en: ['f'],
  },
  full: {
    ru: ['девушка', 'девочка'],
    en: ['girl', 'woman', 'female'],
  },
  sex: {
    ru: ['женского'],
    en: ['female'],
  },
};

function sendSexAnswer(textLower, currentSession, {
  withI,
  classifier,
}) {
  const word = getRandomItem(sexAnswers[classifier][currentSession.locale]);

  if (withI) {
    switch (currentSession.locale) {
      case 'en':
        return `I'm ${word}`;

      case 'ru':
        return `я ${word}`;

      default:
        return word;
    }

    return `я ${word}`;
  }

  return word;
}

export default {
  check: isSexDetectQuestion,
  canRepeat: true,
  answer: sendSexAnswer,
};
