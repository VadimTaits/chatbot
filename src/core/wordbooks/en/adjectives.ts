import type {
  AdjectiveType,
} from 'core/types';

export const adjectives: AdjectiveType[] = [
  [
    ['cute'],
    ['cute'],
    ['cute'],
    ['cute'],
  ],
];
