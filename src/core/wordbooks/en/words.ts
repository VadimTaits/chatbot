import type {
  WordType,
} from 'core/types';

// TO DO
const words: WordType[] = [
  [
    [
      "fork",
      "fork",
      "fork"
    ],
    [
      "forks",
      "forks",
      "forks"
    ],
    {
      live: false,
      sex: 'a',
      containable: 'none',
    },
  ],
  [
    [
      "spoon",
      "spoon",
      "spoon"
    ],
    [
      "spoons",
      "spoons",
      "spoons"
    ],
    {
      live: false,
      sex: 'a',
      containable: 'none',
    },
  ],
  [
    [
      "wand",
      "wand",
      "wand"
    ],
    [
      "wands",
      "wands",
      "wands"
    ],
    {
      live: false,
      sex: 'a',
      containable: 'none',
    },
  ],
  [
    [
      "pillow",
      "pillow",
      "pillow"
    ],
    [
      "pillows",
      "pillows",
      "pillows"
    ],
    {
      live: false,
      sex: 'a',
      containable: 'none',
    },
  ],
  [
    [
      "sand",
      "sand",
      "sand"
    ],
    [
      "sands",
      "sands",
      "sands"
    ],
    {
      live: false,
      sex: 'a',
      containable: 'small',
    },
  ],
  [
    [
      "clay",
      "clay",
      "clay"
    ],
    [
      "clays",
      "clays",
      "clays"
    ],
    {
      live: false,
      sex: 'a',
      containable: 'small',
    },
  ],
  [
    [
      "leaf",
      "leaf",
      "leaf"
    ],
    [
      "leafs",
      "leafs",
      "leafs"
    ],
    {
      live: false,
      sex: 'a',
      containable: 'none',
    },
  ],
  [
    [
      "eye",
      "eye",
      "eye"
    ],
    [
      "eyes",
      "eyes",
      "eyes"
    ],
    {
      live: false,
      sex: 'a',
      containable: 'none',
    },
  ],
  [
    [
      "nose",
      "nose",
      "nose"
    ],
    [
      "noses",
      "noses",
      "noses"
    ],
    {
      live: false,
      sex: 'a',
      containable: 'small',
    },
  ],
  [
    [
      "ear",
      "ear",
      "ear"
    ],
    [
      "ears",
      "ears",
      "ears"
    ],
    {
      live: false,
      sex: 'a',
      containable: 'small',
    },
  ],
  [
    [
      "hand",
      "hand",
      "hand"
    ],
    [
      "hands",
      "hands",
      "hands"
    ],
    {
      live: false,
      sex: 'a',
      containable: 'none',
    },
  ],
  [
    [
      "leg",
      "leg",
      "leg"
    ],
    [
      "legs",
      "legs",
      "legs"
    ],
    {
      live: false,
      sex: 'a',
      containable: 'none',
    },
  ],
  [
    [
      "eyebrow",
      "eyebrow",
      "eyebrow"
    ],
    [
      "eyebrows",
      "eyebrows",
      "eyebrows"
    ],
    {
      live: false,
      sex: 'a',
      containable: 'none',
    },
  ],
  [
    [
      "horn",
      "horn",
      "horn"
    ],
    [
      "horns",
      "horns",
      "horns"
    ],
    {
      live: false,
      sex: 'a',
      containable: 'none',
    },
  ],
  [
    [
      "hoof",
      "hoof",
      "hoof"
    ],
    [
      "hoofs",
      "hoofs",
      "hoofs"
    ],
    {
      live: false,
      sex: 'a',
      containable: 'none',
    },
  ],
  [
    [
      "cat",
      "cat",
      "cat"
    ],
    [
      "cats",
      "cats",
      "cats"
    ],
    {
      live: true,
      sex: 'a',
      containable: 'none',
    },
  ],
  [
    [
      "dog",
      "dog",
      "dog"
    ],
    [
      "dogs",
      "dogs",
      "dogs"
    ],
    {
      live: true,
      sex: 'a',
      containable: 'none',
    },
  ],
  [
    [
      "mole",
      "mole",
      "mole"
    ],
    [
      "moles",
      "moles",
      "moles"
    ],
    {
      live: true,
      sex: 'a',
      containable: 'none',
    },
  ],
  [
    [
      "ostrich",
      "ostrich",
      "ostrich"
    ],
    [
      "ostrichs",
      "ostrichs",
      "ostrichs"
    ],
    {
      live: true,
      sex: 'a',
      containable: 'none',
    },
  ],
  [
    [
      "elephant",
      "elephant",
      "elephant"
    ],
    [
      "elephants",
      "elephants",
      "elephants"
    ],
    {
      live: true,
      sex: 'a',
      containable: 'none',
    },
  ],
  [
    [
      "turtle",
      "turtle",
      "turtle"
    ],
    [
      "turtles",
      "turtles",
      "turtles"
    ],
    {
      live: true,
      sex: 'a',
      containable: 'none',
    },
  ],
  [
    [
      "tortoise",
      "tortoise",
      "tortoise"
    ],
    [
      "tortoises",
      "tortoises",
      "tortoises"
    ],
    {
      live: true,
      sex: 'a',
      containable: 'none',
    },
  ],
  [
    [
      "rabbit",
      "rabbit",
      "rabbit"
    ],
    [
      "rabbits",
      "rabbits",
      "rabbits"
    ],
    {
      live: true,
      sex: 'a',
      containable: 'none',
    },
  ],
  [
    [
      "shirt",
      "shirt",
      "shirt"
    ],
    [
      "shirts",
      "shirts",
      "shirts"
    ],
    {
      live: false,
      sex: 'a',
      containable: 'none',
    },
  ],
  [
    [
      "jeans",
      "jeans",
      "jeans"
    ],
    [
      "jeanses",
      "jeanses",
      "jeanses"
    ],
    {
      live: false,
      sex: 'a',
      containable: 'small',
    },
  ],
  [
    [
      "shoes",
      "shoes",
      "shoes"
    ],
    [
      "shoeses",
      "shoeses",
      "shoeses"
    ],
    {
      live: false,
      sex: 'a',
      containable: 'small',
    },
  ],
  [
    [
      "sleeve",
      "sleeve",
      "sleeve"
    ],
    [
      "sleeves",
      "sleeves",
      "sleeves"
    ],
    {
      live: false,
      sex: 'a',
      containable: 'small',
    },
  ],
  [
    [
      "bicycle",
      "bicycle",
      "bicycle"
    ],
    [
      "bicycles",
      "bicycles",
      "bicycles"
    ],
    {
      live: false,
      sex: 'a',
      containable: 'none',
    },
  ],
  [
    [
      "skate",
      "skate",
      "skate"
    ],
    [
      "skates",
      "skates",
      "skates"
    ],
    {
      live: false,
      sex: 'a',
      containable: 'none',
    },
  ],
  [
    [
      "museum",
      "museum",
      "museum"
    ],
    [
      "museums",
      "museums",
      "museums"
    ],
    {
      live: false,
      sex: 'a',
      containable: 'big',
    },
  ],
  [
    [
      "phone",
      "phone",
      "phone"
    ],
    [
      "phones",
      "phones",
      "phones"
    ],
    {
      live: false,
      sex: 'a',
      containable: 'none',
    },
  ],
  [
    [
      "worm",
      "worm",
      "worm"
    ],
    [
      "worms",
      "worms",
      "worms"
    ],
    {
      live: true,
      sex: 'a',
      containable: 'none',
    },
  ],
  [
    [
      "spider",
      "spider",
      "spider"
    ],
    [
      "spiders",
      "spiders",
      "spiders"
    ],
    {
      live: true,
      sex: 'a',
      containable: 'none',
    },
  ],
  [
    [
      "ant",
      "ant",
      "ant"
    ],
    [
      "ants",
      "ants",
      "ants"
    ],
    {
      live: true,
      sex: 'a',
      containable: 'none',
    },
  ],
  [
    [
      "dragonfly",
      "dragonfly",
      "dragonfly"
    ],
    [
      "dragonflys",
      "dragonflys",
      "dragonflys"
    ],
    {
      live: true,
      sex: 'a',
      containable: 'none',
    },
  ],
  [
    [
      "cube",
      "cube",
      "cube"
    ],
    [
      "cubes",
      "cubes",
      "cubes"
    ],
    {
      live: false,
      sex: 'a',
      containable: 'none',
    },
  ],
  [
    [
      "triangle",
      "triangle",
      "triangle"
    ],
    [
      "triangles",
      "triangles",
      "triangles"
    ],
    {
      live: false,
      sex: 'a',
      containable: 'none',
    },
  ],
  [
    [
      "cube",
      "cube",
      "cube"
    ],
    [
      "cubes",
      "cubes",
      "cubes"
    ],
    {
      live: false,
      sex: 'a',
      containable: 'none',
    },
  ],
  [
    [
      "boat",
      "boat",
      "boat"
    ],
    [
      "boats",
      "boats",
      "boats"
    ],
    {
      live: false,
      sex: 'a',
      containable: 'small',
    },
  ],
  [
    [
      "vessel",
      "vessel",
      "vessel"
    ],
    [
      "vessels",
      "vessels",
      "vessels"
    ],
    {
      live: false,
      sex: 'a',
      containable: 'small',
    },
  ],
  [
    [
      "ship",
      "ship",
      "ship"
    ],
    [
      "ships",
      "ships",
      "ships"
    ],
    {
      live: false,
      sex: 'a',
      containable: 'big',
    },
  ],
  [
    [
      "train",
      "train",
      "train"
    ],
    [
      "trains",
      "trains",
      "trains"
    ],
    {
      live: false,
      sex: 'a',
      containable: 'big',
    },
  ],
  [
    [
      "soup",
      "soup",
      "soup"
    ],
    [
      "soups",
      "soups",
      "soups"
    ],
    {
      live: false,
      sex: 'a',
      containable: 'small',
    },
  ],
  [
    [
      "bouillon",
      "bouillon",
      "bouillon"
    ],
    [
      "bouillons",
      "bouillons",
      "bouillons"
    ],
    {
      live: false,
      sex: 'a',
      containable: 'small',
    },
  ],
  [
    [
      "borscht",
      "borscht",
      "borscht"
    ],
    [
      "borschts",
      "borschts",
      "borschts"
    ],
    {
      live: false,
      sex: 'a',
      containable: 'small',
    },
  ],
  [
    [
      "linen",
      "linen",
      "linen"
    ],
    [
      "linens",
      "linens",
      "linens"
    ],
    {
      live: false,
      sex: 'a',
      containable: 'small',
    },
  ],
  [
    [
      "object",
      "object",
      "object"
    ],
    [
      "objects",
      "objects",
      "objects"
    ],
    {
      live: false,
      sex: 'a',
      containable: 'none',
    },
  ],
  [
    [
      "lens",
      "lens",
      "lens"
    ],
    [
      "lenses",
      "lenses",
      "lenses"
    ],
    {
      live: false,
      sex: 'a',
      containable: 'none',
    },
  ],
  [
    [
      "drawing",
      "drawing",
      "drawing"
    ],
    [
      "drawings",
      "drawings",
      "drawings"
    ],
    {
      live: false,
      sex: 'a',
      containable: 'none',
    },
  ],
  [
    [
      "image",
      "image",
      "image"
    ],
    [
      "images",
      "images",
      "images"
    ],
    {
      live: false,
      sex: 'a',
      containable: 'none',
    },
  ],
  [
    [
      "potatoes",
      "potatoes",
      "potatoes"
    ],
    [
      "potatoeses",
      "potatoeses",
      "potatoeses"
    ],
    {
      live: false,
      sex: 'a',
      containable: 'none',
    },
  ],
  [
    [
      "onion",
      "onion",
      "onion"
    ],
    [
      "onions",
      "onions",
      "onions"
    ],
    {
      live: false,
      sex: 'a',
      containable: 'none',
    },
  ],
  [
    [
      "bow",
      "bow",
      "bow"
    ],
    [
      "bows",
      "bows",
      "bows"
    ],
    {
      live: false,
      sex: 'a',
      containable: 'none',
    },
  ],
  [
    [
      "sun",
      "sun",
      "sun"
    ],
    [
      "suns",
      "suns",
      "suns"
    ],
    {
      live: false,
      sex: 'a',
      containable: 'none',
    },
  ],
  [
    [
      "moon",
      "moon",
      "moon"
    ],
    [
      "moons",
      "moons",
      "moons"
    ],
    {
      live: false,
      sex: 'a',
      containable: 'none',
    },
  ],
  [
    [
      "star",
      "star",
      "star"
    ],
    [
      "stars",
      "stars",
      "stars"
    ],
    {
      live: false,
      sex: 'a',
      containable: 'none',
    },
  ],
  [
    [
      "kefir",
      "kefir",
      "kefir"
    ],
    [
      "kefirs",
      "kefirs",
      "kefirs"
    ],
    {
      live: false,
      sex: 'a',
      containable: 'small',
    },
  ],
  [
    [
      "bug",
      "bug",
      "bug"
    ],
    [
      "bugs",
      "bugs",
      "bugs"
    ],
    {
      live: true,
      sex: 'a',
      containable: 'none',
    },
  ],
  [
    [
      "cup",
      "cup",
      "cup"
    ],
    [
      "cups",
      "cups",
      "cups"
    ],
    {
      live: false,
      sex: 'a',
      containable: 'small',
    },
  ],
  [
    [
      "napkin",
      "napkin",
      "napkin"
    ],
    [
      "napkins",
      "napkins",
      "napkins"
    ],
    {
      live: false,
      sex: 'a',
      containable: 'none',
    },
  ],
  [
    [
      "cock",
      "cock",
      "cock"
    ],
    [
      "cocks",
      "cocks",
      "cocks"
    ],
    {
      live: true,
      sex: 'a',
      containable: 'none',
    },
  ],
  [
    [
      "hen",
      "hen",
      "hen"
    ],
    [
      "hens",
      "hens",
      "hens"
    ],
    {
      live: true,
      sex: 'a',
      containable: 'none',
    },
  ],
  [
    [
      "chicken",
      "chicken",
      "chicken"
    ],
    [
      "chickens",
      "chickens",
      "chickens"
    ],
    {
      live: true,
      sex: 'a',
      containable: 'none',
    },
  ],
  [
    [
      "penguin",
      "penguin",
      "penguin"
    ],
    [
      "penguins",
      "penguins",
      "penguins"
    ],
    {
      live: true,
      sex: 'a',
      containable: 'none',
    },
  ],
  [
    [
      "boot",
      "boot",
      "boot"
    ],
    [
      "boots",
      "boots",
      "boots"
    ],
    {
      live: false,
      sex: 'a',
      containable: 'small',
    },
  ],
  [
    [
      "sign",
      "sign",
      "sign"
    ],
    [
      "signs",
      "signs",
      "signs"
    ]
  ],
  [
    [
      "snow",
      "snow",
      "snow"
    ],
    [
      "snows",
      "snows",
      "snows"
    ]
  ],
  [
    [
      "house",
      "house",
      "house"
    ],
    [
      "houses",
      "houses",
      "houses"
    ]
  ],
  [
    [
      "door",
      "door",
      "door"
    ],
    [
      "doors",
      "doors",
      "doors"
    ]
  ],
  [
    [
      "map",
      "map",
      "map"
    ],
    [
      "maps",
      "maps",
      "maps"
    ]
  ],
  [
    [
      "bone",
      "bone",
      "bone"
    ],
    [
      "bones",
      "bones",
      "bones"
    ]
  ],
  [
    [
      "ball",
      "ball",
      "ball"
    ],
    [
      "balls",
      "balls",
      "balls"
    ]
  ],
  [
    [
      "bullet",
      "bullet",
      "bullet"
    ],
    [
      "bullets",
      "bullets",
      "bullets"
    ]
  ],
  [
    [
      "garden",
      "garden",
      "garden"
    ],
    [
      "gardens",
      "gardens",
      "gardens"
    ]
  ],
  [
    [
      "knock",
      "knock",
      "knock"
    ],
    [
      "knocks",
      "knocks",
      "knocks"
    ]
  ],
  [
    [
      "market",
      "market",
      "market"
    ],
    [
      "markets",
      "markets",
      "markets"
    ]
  ],
  [
    [
      "melon",
      "melon",
      "melon"
    ],
    [
      "melons",
      "melons",
      "melons"
    ]
  ],
  [
    [
      "tea",
      "tea",
      "tea"
    ],
    [
      "teas",
      "teas",
      "teas"
    ]
  ],
  [
    [
      "coffee",
      "coffee",
      "coffee"
    ],
    [
      "coffees",
      "coffees",
      "coffees"
    ]
  ],
  [
    [
      "milk",
      "milk",
      "milk"
    ],
    [
      "milks",
      "milks",
      "milks"
    ]
  ],
  [
    [
      "ice",
      "ice",
      "ice"
    ],
    [
      "ices",
      "ices",
      "ices"
    ]
  ],
  [
    [
      "icecream",
      "icecream",
      "icecream"
    ],
    [
      "icecreams",
      "icecreams",
      "icecreams"
    ]
  ],
  [
    [
      "truck",
      "truck",
      "truck"
    ],
    [
      "trucks",
      "trucks",
      "trucks"
    ]
  ],
  [
    [
      "car",
      "car",
      "car"
    ],
    [
      "cars",
      "cars",
      "cars"
    ]
  ],
  [
    [
      "flag",
      "flag",
      "flag"
    ],
    [
      "flags",
      "flags",
      "flags"
    ]
  ],
  [
    [
      "neighbor",
      "neighbor",
      "neighbor"
    ],
    [
      "neighbors",
      "neighbors",
      "neighbors"
    ]
  ],
  [
    [
      "key",
      "key",
      "key"
    ],
    [
      "keys",
      "keys",
      "keys"
    ]
  ],
  [
    [
      "nut",
      "nut",
      "nut"
    ],
    [
      "nuts",
      "nuts",
      "nuts"
    ]
  ],
  [
    [
      "nail",
      "nail",
      "nail"
    ],
    [
      "nails",
      "nails",
      "nails"
    ]
  ],
  [
    [
      "raspberries",
      "raspberries",
      "raspberries"
    ],
    [
      "raspberrieses",
      "raspberrieses",
      "raspberrieses"
    ]
  ],
  [
    [
      "pencil",
      "pencil",
      "pencil"
    ],
    [
      "pencils",
      "pencils",
      "pencils"
    ]
  ],
  [
    [
      "elastic",
      "elastic",
      "elastic"
    ],
    [
      "elastics",
      "elastics",
      "elastics"
    ]
  ],
  [
    [
      "ram",
      "ram",
      "ram"
    ],
    [
      "rams",
      "rams",
      "rams"
    ]
  ],
  [
    [
      "sheep",
      "sheep",
      "sheep"
    ],
    [
      "sheeps",
      "sheeps",
      "sheeps"
    ]
  ],
  [
    [
      "author",
      "author",
      "author"
    ],
    [
      "authors",
      "authors",
      "authors"
    ]
  ],
  [
    [
      "school",
      "school",
      "school"
    ],
    [
      "schools",
      "schools",
      "schools"
    ]
  ],
  [
    [
      "factory",
      "factory",
      "factory"
    ],
    [
      "factorys",
      "factorys",
      "factorys"
    ]
  ],
  [
    [
      "department",
      "department",
      "department"
    ],
    [
      "departments",
      "departments",
      "departments"
    ]
  ],
  [
    [
      "site",
      "site",
      "site"
    ],
    [
      "sites",
      "sites",
      "sites"
    ]
  ],
  [
    [
      "window",
      "window",
      "window"
    ],
    [
      "windows",
      "windows",
      "windows"
    ]
  ],
  [
    [
      "street",
      "street",
      "street"
    ],
    [
      "streets",
      "streets",
      "streets"
    ]
  ],
  [
    [
      "rose",
      "rose",
      "rose"
    ],
    [
      "roses",
      "roses",
      "roses"
    ]
  ],
  [
    [
      "flower",
      "flower",
      "flower"
    ],
    [
      "flowers",
      "flowers",
      "flowers"
    ]
  ]
];

export default words;
