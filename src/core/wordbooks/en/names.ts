import _manNames from './man-names.json';
import _womanNames from './woman-names.json';

export const manNames = _manNames;
export const womanNames = _womanNames;

export const manNamesLower = manNames
  .map((namesGroup) => namesGroup.map((name) => name.toLowerCase()));

export const manNamesToGroupsMap = manNamesLower.reduce((res, namesGroup, groupIndex) => {
  namesGroup.forEach((name) => {
    res[name] = groupIndex;
  });

  return res;
}, {});

export const manNamesLowerFlatten = Object.keys(manNamesToGroupsMap);

export const womanNamesLower = womanNames
  .map((namesGroup) => namesGroup.map((name) => name.toLowerCase()));

export const womanNamesToGroupsMap = womanNamesLower.reduce((res, namesGroup, groupIndex) => {
  namesGroup.forEach((name) => {
    res[name] = groupIndex;
  });

  return res;
}, {});

export const womanNamesLowerFlatten = Object.keys(womanNamesToGroupsMap);
