import en from '../en/words';
import ru from '../ru/words';

export default {
  ru,
  en,
};
