import type {
  LocaleType,
  AdjectiveType,
} from 'core/types';

import { adjectives as en } from '../en/adjectives';
import { adjectives as ru } from '../ru/adjectives';

export default {
  en,
  ru,
} as Record<LocaleType, AdjectiveType[]>;
