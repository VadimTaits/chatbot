import type {
  LocaleType,
  VerbType,
} from 'core/types';

import {
  verbs as ru,
} from '../ru/verbs';

export default {
  en: [] as VerbType[],
  ru,
} as Record<LocaleType, VerbType[]>;
