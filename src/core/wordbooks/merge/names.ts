import * as en from '../en/names';
import * as ru from '../ru/names';

export default {
  en,
  ru,
};
