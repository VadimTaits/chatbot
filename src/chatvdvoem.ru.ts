import initChat from './core';

const textInputEl = document.getElementById('text');
const sendButtonEl = document.getElementById('but-send');
const typingIndicatorEl = document.getElementById('typing');

function sendMessage(message) {
  textInputEl.textContent = message;
  sendButtonEl.click();
}

function getNewMessages(loadedMessagesLength) {
  const messagesEls = document.querySelectorAll('#log > ol > li');

  const newMessagesEls = Array.prototype.filter.call(
    messagesEls,
    (messageEl, index) => index >= loadedMessagesLength,
  );

  return newMessagesEls.map((newMessageEl) => {
    const contentEl = newMessageEl.querySelector('.message');

    return {
      time: newMessageEl.dataset.time,
      isOwn: newMessageEl.classList.contains('to'),
      text: contentEl ?
        contentEl.textContent :
        '*стикер*',
    };
  });
}

function isChatEnded() {
  return !!document.querySelector('#log .ended');
}

function isStatsAllowed() {
  return true;
}

function getOpponentHash() {
  return chat.opponent.guid;
}

function tryStartNewChat() {
  const startBtnEl = document.getElementById('but-start');

  if (startBtnEl) {
    startBtnEl.click();

    return true;
  }

  return false;
}

function isOpoSendAllowed() {
  return !!chat.opponent.guid;
}

function typeLetter() {
  $('#text').trigger('keyup');
}

initChat({
  sendMessage,
  getNewMessages,
  isChatEnded,
  tryStartNewChat,

  close: () => {
    const closeBtnEl = document.getElementById('but-close');
  
    if (closeBtnEl) {
      closeBtnEl.click();
  
      return true;
    }
  
    return false;
  },

  isOpoSendAllowed,

  sendOpoMessage: (message) => {
    chat.sendSocketMessage({
      action: 'send_message',
      message,
      uid: chat.opponent.guid,
      chat: chat.chatId,
    });
  },

  isStatsAllowed,
  getOpponentHash,
  typeLetter,

  checkOpoTyping: () => typingIndicatorEl.style.display === 'block',

  additionalStyle: `
    .show-projects #log {
      right: 0 !important;
    }

    .show-projects #projects {
      display: none !important;
    }

    #text-wrap {
      left: 70px;
    }

    #but-start, #but-close {
      width: 70px;
    }

    #but-start > span, #but-close > span {
      display: none;
    }
  `,

  debug: false,

  locale: 'ru',
});
