## Установка зависимостей

### node (lts-версия)

https://nodejs.org/

### yarn

https://yarnpkg.com/getting-started/install

## Запуск проекта

### Сборка

```bash
yarn build
```

### Запуск сервера

```bash
yarn start
```

### Подключение скрипта на сайт

Открыть чат, начать диалог (пройти капчи и т.п.), открыть девтулз, вставить туда содержимое файла `inject.js`

